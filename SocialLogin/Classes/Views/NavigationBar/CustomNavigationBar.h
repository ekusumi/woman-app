//
//  CustomNavigationBar.h
//  HealthInHand
//
//  Created by Razvan on 12/17/12.
//  Copyright (c) 2012 Creatio. All rights reserved.
//

#import <UIKit/UIKit.h>

// =============================================================================
// CustomNavigationBar Public Interface
// =============================================================================
@interface CustomNavigationBar : UINavigationController

//Public methods
- (void)showLogo:(BOOL)shouldShow;
    
@end
