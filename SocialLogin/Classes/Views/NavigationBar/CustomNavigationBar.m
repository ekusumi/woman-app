//
//  CustomNavigationBar.m
//  HealthInHand
//
//  Created by Razvan on 12/17/12.
//  Copyright (c) 2012 Creatio. All rights reserved.
//

#import "CustomNavigationBar.h"

// =============================================================================
// CustomNavigationBar Implementation
// =============================================================================
@implementation CustomNavigationBar

// -----------------------------------------------------------------------------
// Public methods
// -----------------------------------------------------------------------------
#pragma mark - Public methods
- (void)showLogo:(BOOL)shouldShow {
    if (shouldShow) {
        [self setBackgroundImage:[UIImage imageNamed:@"nav_bar.png"]
                   forBarMetrics:UIBarMetricsDefault];
    } else {
        [self setBackgroundImage:[UIImage imageNamed:@"nav_bar.png"]
                   forBarMetrics:UIBarMetricsDefault];
    }
}

@end
