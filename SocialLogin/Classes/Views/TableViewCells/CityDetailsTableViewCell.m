//
//  CityDetailsTableViewCell.m
//  SocialLogin
//
//  Created by Razvan on 7/22/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import "CityDetailsTableViewCell.h"

@implementation CityDetailsTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateWithInfo:(NSDictionary *) info {
    [self.name setText:[info objectForKey:@"name"]];
    [self.price setText:[info objectForKey:@"price"]];
    [self.time setText:[info objectForKey:@"time"]];
    [self.address setText:[info objectForKey:@"address"]];
    [self.station setText:[info objectForKey:@"station"]];
    
    [self.btnTel.layer setCornerRadius:10.0];
    [self.btnTel.layer setBorderColor:[[UIColor clearColor] CGColor]];
    
    [self.btnWeb.layer setCornerRadius:10.0];
    [self.btnWeb.layer setBorderColor:[[UIColor clearColor] CGColor]];
}

@end
