//
//  FriendsCell.m
//  SocialLogin
//
//  Created by sebastian.achim on 7/10/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import "FriendsCell.h"

@implementation FriendsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.cellBackground = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 55.0)];
    [self.cellBackground setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"tableView_bkgd.png"]]];
    [self addSubview: self.cellBackground];
    [self.cellBackground release];
    
    self.bannerImage = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 310.0, 80.0)];
    [self.bannerImage setContentMode:UIViewContentModeCenter];
    [self addSubview:self.bannerImage];
    [self.bannerImage release];
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateWithBanners:(FriendsBanners *)banners {
    self.friendsBanners = banners;
    
    [self.bannerImage setImage:[UIImage imageNamed:@"banner_test.png"]];
}

@end
