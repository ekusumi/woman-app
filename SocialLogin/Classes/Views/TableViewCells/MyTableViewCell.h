//
//  MyTableViewCell.h
//  SocialLogin
//
//  Created by Me on 8/6/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UIImageView *imageV;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UILabel *subtitle1Label;
@property (retain, nonatomic) IBOutlet UILabel *subtitle2Label;

@end
