//
//  CityDetailsTableViewCell.h
//  SocialLogin
//
//  Created by Razvan on 7/22/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface CityDetailsTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *name, *price, *address, *time, *station;
@property (nonatomic, retain) IBOutlet UIButton *btnWeb, *btnTel;

- (void)updateWithInfo:(NSDictionary *) info;

@end
