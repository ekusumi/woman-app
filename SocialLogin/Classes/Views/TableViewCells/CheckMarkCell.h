//
//  CheckMarkCell.h
//  SocialLogin
//
//  Created by sebastian.achim on 7/17/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckMarkCell : UITableViewCell

@property (retain, nonatomic)IBOutlet  UIImageView    *rowBackground;
@property (retain, nonatomic)IBOutlet  UILabel        *titleLabel;


@end
