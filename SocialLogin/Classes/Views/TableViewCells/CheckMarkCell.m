//
//  CheckMarkCell.m
//  SocialLogin
//
//  Created by sebastian.achim on 7/17/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import "CheckMarkCell.h"

@implementation CheckMarkCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
