//
//  UserTableViewCell.m
//  Emerald
//
//  Created by Razvan on 6/11/13.
//  Copyright (c) 2013 coltboy. All rights reserved.
//

#import "UserTableViewCell.h"

//=======================================================================
// UserTableViewCell - Implementation
//=======================================================================
@implementation UserTableViewCell

- (id)init {
    self = [super init];
    if (self) {
        // Initialization code

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateWithInformation:(UserInfo*)userInfo {
    [self.titleLabel setText:userInfo.title];
    [self.subtitleLabel setText:[NSString stringWithFormat:@"Deadline:\n%@", userInfo.deadline]];
    [self.pointsLabel setText:userInfo.points];
    
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(concurrentQueue, ^{
        NSData *image = [NSData dataWithContentsOfURL:[NSURL URLWithString:userInfo.imageURL]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.imageV setImage:[UIImage imageWithData:image]];
            [self.imageV.layer setCornerRadius:10.0];
            [self.imageV.layer setBorderColor:[[UIColor clearColor] CGColor]];
            self.imageV.clipsToBounds = YES;
        });
    });
}

@end