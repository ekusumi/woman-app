//
//  FriendsCell.h
//  SocialLogin
//
//  Created by sebastian.achim on 7/10/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendsBanners.h"

@interface FriendsCell : UITableViewCell

@property (nonatomic, retain) UIImageView      *bannerImage;
@property (nonatomic, retain) UIView           *cellBackground;

@property (nonatomic, retain) FriendsBanners   *friendsBanners;

- (void)updateWithBanners:(FriendsBanners *)banners;

@end
