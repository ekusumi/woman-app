//
//  SearchInfo.h
//  SocialLogin
//
//  Created by sebastian.achim on 8/15/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchInfo : NSObject

@property (nonatomic, retain) NSString  *selectionControllerInfo;
@property (nonatomic, retain) NSString  *locationControllerInfo;
@property (nonatomic, retain) NSString  *chatControllerInfo;
@property (nonatomic, retain) NSString  *stationControllerInfo;

@property (nonatomic, retain) NSString *location;
@property (nonatomic, retain) NSString *selection;
@property (nonatomic, retain) NSString *chat;
@property (nonatomic, retain) NSString *station;

+ (SearchInfo *)sharedInstance;

- (void)clearAllInfo;
- (NSString *)stringFromArray:(NSArray *)infoArray;

@end
