//
//  SearchInfo.m
//  SocialLogin
//
//  Created by sebastian.achim on 8/15/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import "SearchInfo.h"

static SearchInfo  *kSearchInfo   = nil;


@implementation SearchInfo

- (id)init {
    
    self = [super init];
    if(!self) return nil;
        
    return self;
}

+ (SearchInfo *)sharedInstance {
    if(!kSearchInfo) {
        kSearchInfo = [[SearchInfo alloc] init];
    }
    return kSearchInfo;
}

- (void)clearAllInfo {
    self.selectionControllerInfo = @"";
    self.stationControllerInfo = @"";
    self.chatControllerInfo = @"";
    self.locationControllerInfo = @"";
    
    self.location = @"";
    self.station = @"";
    self.chat = @"";
    self.selection = @"";
}

- (NSString *)stringFromArray:(NSArray *)infoArray {
    NSString *composed = @"";
    for (int i=0; i<infoArray.count; i++) {
        if (i == infoArray.count -1) {
            composed = [composed stringByAppendingString:infoArray[i]];
        } else {
            composed = [composed stringByAppendingFormat:@"%@,",infoArray[i]];
        }
    }
    
    return composed;
}

@end
