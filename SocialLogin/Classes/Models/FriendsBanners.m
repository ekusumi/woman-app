//
//  FriendsBanners.m
//  SocialLogin
//
//  Created by sebastian.achim on 7/10/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import "FriendsBanners.h"

@implementation FriendsBanners


- (id)initWithBanners:(NSDictionary *)info {
    self = [super init];
    if (!self) return nil;
    
    self.bannersURL = [info objectForKey:@"banner"];
    
    return self;
}

- (void)dealloc {
    
    self.bannersURL = nil;
    
    [super dealloc];
}

@end
