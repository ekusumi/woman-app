//
//  CityDetailsViewController.m
//  SocialLogin
//
//  Created by Razvan on 7/22/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import "CityDetailsViewController.h"
#import "CityDetailsTableViewCell.h"
#import "UserManager.h"
#import "DataManager.h"
#import "SVProgressHUD.h"
#import "MoreDetailsViewController.h"
#import "SearchInfo.h"

@interface CityDetailsViewController ()

@end

@implementation CityDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar.png"] forBarMetrics:UIBarMetricsDefault];
    
    //back button from details view to hotOffers view
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 54.0, 35.0)];
    [backButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    [backButton setTitle:NSLocalizedString(@"", nil) forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    
    _backBarBtn = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton release];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationItem setLeftBarButtonItem:_backBarBtn animated:YES];
    self.navigationItem.title = @"Search";

    
    // add loading view
//    if (!_loadingView) {
//        
//        LoadingView *loadingObject = [[LoadingView alloc] init];
//        _loadingView = [loadingObject view];
//        [self.view addSubview:_loadingView];
//        [_loadingView centerContents];
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
//    [self.loadingView show];
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeGradient];
    [self retrieveData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

}

- (void)viewWillDisappear:(BOOL)animated {

}

//=======================================================================
// Data Handlers
//=======================================================================
#pragma mark - Data Handlers
- (void)retrieveData {
    //    [self.userTableView setHidden:YES];
    //    [self.pointsLabel setHidden:YES];
    
    //    [self.infoArray removeAllObjects];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSString *userId     = [[UserManager sharedInstance] userId];
    NSString *userToken  = [[UserManager sharedInstance] userToken];
    
    [params setObject:!userId ? @"":userId forKey:@"id"];
    [params setObject:!userToken ? @"":userToken forKey:@"token"];
    
    [params setObject:[[SearchInfo sharedInstance] location] forKey:@"location"];
    [params setObject:[[SearchInfo sharedInstance] locationControllerInfo] forKey:@"locationInfo"];
    
    [params setObject:[[SearchInfo sharedInstance] selection] forKey:@"selection"];
    [params setObject:[[SearchInfo sharedInstance] selectionControllerInfo] forKey:@"selectionInfo"];
    
    [params setObject:[[SearchInfo sharedInstance] station] forKey:@"station"];
    [params setObject:[[SearchInfo sharedInstance] stationControllerInfo] forKey:@"stationInfo"];
    
    [params setObject:[[SearchInfo sharedInstance] chat] forKey:@"chat"];
    [params setObject:[[SearchInfo sharedInstance] chatControllerInfo] forKey:@"chatInfo"];
    
    NSLog(@"PARAM %@", params);
    
    [[DataManager sharedInstance] postRequestWithParameters:params
                                                     atPath:URL_Search
                                                    inBlock:^(id results){
                                                        _infoArray = [[NSArray alloc] initWithArray:[results objectForKey:@"game"]];
                                                        NSLog(@"COUNT %d", [_infoArray count]);
                                                        [_tableView reloadData];
                                                        [SVProgressHUD dismiss];
                                                        [self.loadingView hide];
                                                    }];
}

//=======================================================================
// UITableViewDataSource methods
//=======================================================================
#pragma mark - UITableViewDataSource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CityDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CityCustomCell"];
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CityDetailsTableViewCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSDictionary *info = [_infoArray objectAtIndex:indexPath.section];
    NSDictionary *event = [info objectForKey:@"Event"];
    [cell updateWithInfo:event];
    
    [cell.btnTel addTarget:self action:@selector(onSelect) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [_infoArray count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

//=======================================================================
// UITableViewDelegate methods
//=======================================================================
#pragma mark - UITableViewDelegate methods
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, tableView.frame.size.width, 25.0)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, tableView.frame.size.width, 25.0)];
    [label setFont:[UIFont boldSystemFontOfSize:13]];
    [label setTextColor:[UIColor whiteColor]];
    [label setText:[NSString stringWithFormat:@"Section title %d", section +1]];
    [label setBackgroundColor:[UIColor clearColor]];
    
    NSDictionary *info = [_infoArray objectAtIndex:section];
    NSDictionary *event = [info objectForKey:@"Event"];
    [label setText:[event objectForKey:@"name"]];
    
    [view addSubview:label];
    [view setBackgroundColor:APP_TEXT_COLOR_PINK];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 25.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 200.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *info = [_infoArray objectAtIndex:indexPath.section];
    NSDictionary *event = [info objectForKey:@"Event"];
    [self showMoreDetails:[NSString stringWithFormat:@"http://www.tainew.com/shop/view/%@",[event objectForKey:@"id"]]];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *info = [_infoArray objectAtIndex:indexPath.section];
    NSDictionary *event = [info objectForKey:@"Event"];
    [self showMoreDetails:[NSString stringWithFormat:@"http://www.tainew.com/shop/view/%@",[event objectForKey:@"id"]]];
}

- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onSelect {
//    [self showMoreDetails];
}

- (void)showMoreDetails:(NSString *) urlString {
    MoreDetailsViewController *controller = [[MoreDetailsViewController alloc] initWithNibName:@"MoreDetailsViewController" bundle:nil];
    controller.urlString = urlString;
    [self.navigationController pushViewController:controller animated:YES];
}

@end
