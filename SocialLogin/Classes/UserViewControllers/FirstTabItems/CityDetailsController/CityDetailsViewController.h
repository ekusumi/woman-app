//
//  CityDetailsViewController.h
//  SocialLogin
//
//  Created by Razvan on 7/22/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoadingView.h"

@interface CityDetailsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain) LoadingView       *loadingView;
@property (nonatomic, retain) UIBarButtonItem       *backBarBtn;
@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSArray *infoArray;

@end
