//
//  MoreDetailsViewController.h
//  SocialLogin
//
//  Created by Me on 8/20/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>

@interface MoreDetailsViewController : UIViewController <UIWebViewDelegate, MFMailComposeViewControllerDelegate> {
    IBOutlet UIButton *btnWeb, *btnTel, *btnMail;
    IBOutlet UIWebView *webView;
}

@property (nonatomic, retain) NSString *urlString;

- (IBAction)onSendData:(id)sender;
- (IBAction)onWeb;
- (IBAction)onTel;
- (IBAction)onMail;

@end
