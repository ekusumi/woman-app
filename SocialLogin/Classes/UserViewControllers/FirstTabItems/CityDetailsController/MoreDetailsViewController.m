//
//  MoreDetailsViewController.m
//  SocialLogin
//
//  Created by Me on 8/20/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import "MoreDetailsViewController.h"
#import "SVProgressHUD.h"

@interface MoreDetailsViewController ()

@end

@implementation MoreDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    btnWeb.layer.cornerRadius = 10.0f;
    btnTel.layer.cornerRadius = 10.0f;
    btnMail.layer.cornerRadius = 10.0f;
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 54.0, 35.0)];
    [backButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    [backButton setTitle:NSLocalizedString(@"", nil) forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    
    UIBarButtonItem *backBarBtn = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton release];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationItem setLeftBarButtonItem:backBarBtn animated:YES];
    self.navigationItem.title = @"More Details";
    
    [self loadPage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onSendData:(id)sender {
    // What API should we use to send data?
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Social Login"
                                                    message:@"The data has been sent"
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)loadPage {
    NSURL *url = [[NSURL alloc] initWithString:_urlString];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    [webView loadRequest:request];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeGradient];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [SVProgressHUD dismiss];
}

- (IBAction)onWeb {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.tainew.com/shop/view/2508"]];
}

- (IBAction)onTel {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:0364576156"]];
}

- (IBAction)onMail {
    NSArray *toRecipents = [NSArray arrayWithObject:@"kojikagami@ezweb.ne.jp"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setToRecipients:toRecipents];
    
    [self presentViewController:mc animated:YES completion:NULL];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
