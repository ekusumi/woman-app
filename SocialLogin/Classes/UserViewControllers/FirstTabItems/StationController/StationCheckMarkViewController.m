//
//  StationCheckMarkViewController.m
//  SocialLogin
//
//  Created by sebastian.achim on 7/18/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import "StationCheckMarkViewController.h"
#import "CheckMarkCell.h"
#import "CityDetailsViewController.h"
#import "SearchInfo.h"

@interface StationCheckMarkViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong)IBOutlet UITableView *tableView;

@end

@implementation StationCheckMarkViewController

- (id)init {
    self = [super init];
    if (self) {
        // Custom initialization
        self.selectedRowsArray = [NSMutableArray array];
        self.selectedRowsTitles = [NSMutableArray array];
    }
    return self;
}

-(id)initWithBool:(BOOL)isQuickSearch {
    self = [super init];
    if (self) {
        // Custom initialization
        self.selectedRowsArray = [NSMutableArray array];
        self.selectedRowsTitles = [NSMutableArray array];

        self.isQuickSearch = isQuickSearch;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar.png"] forBarMetrics:UIBarMetricsDefault];

    //back button from details view to hotOffers view
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 54.0, 35.0)];
    [backButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    [backButton setTitle:NSLocalizedString(@"", nil) forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    
    _backBarBtn = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton release];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationItem setLeftBarButtonItem:_backBarBtn animated:YES];

    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];

    if (self.cellIndex == 0) {
        //set the navigation bars title
        self.title = [[CheckStation1 componentsSeparatedByString:@";"] objectAtIndex:self.cellIndex];
    } else if (self.cellIndex == 1) {
        //set the navigation bars title
        self.title = [[CheckStation2 componentsSeparatedByString:@";"] objectAtIndex:self.cellIndex];
    } else if (self.cellIndex == 2) {
        //set the navigation bars title
        self.title = [[CheckStation3 componentsSeparatedByString:@";"] objectAtIndex:self.cellIndex];
    } else if (self.cellIndex == 3) {
        //set the navigation bars title
        self.title = [[CheckStation4 componentsSeparatedByString:@";"] objectAtIndex:self.cellIndex];
    } else {
        //set the navigation bars title
        self.title = [[CheckStation4 componentsSeparatedByString:@";"] objectAtIndex:self.cellIndex];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger numberOfRows;
    
    if (self.cellIndex == 0) {
        numberOfRows = [CheckStation1 componentsSeparatedByString:@";"].count;
    } else if (self.cellIndex == 1) {
        numberOfRows = [CheckStation2 componentsSeparatedByString:@";"].count;
    } else if (self.cellIndex == 2) {
        numberOfRows = [CheckStation3 componentsSeparatedByString:@";"].count;
    } else if (self.cellIndex == 3) {
        numberOfRows = [CheckStation4 componentsSeparatedByString:@";"].count;
    } else {
        numberOfRows = [CheckStation5 componentsSeparatedByString:@";"].count;
    }
    
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CheckMarkCell *cell = [tableView dequeueReusableCellWithIdentifier:@"checkmarkCell"];
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CheckMarkCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    NSString *cellText;
    
    if (self.cellIndex == 0) {
        cellText = [self returnStringWithBase:CheckStation1 forIndex:indexPath.row];
    } else if (self.cellIndex == 1) {
        cellText = [self returnStringWithBase:CheckStation2 forIndex:indexPath.row];
    } else if (self.cellIndex == 2) {
        cellText = [self returnStringWithBase:CheckStation3 forIndex:indexPath.row];
    } else if (self.cellIndex == 3) {
        cellText = [self returnStringWithBase:CheckStation4 forIndex:indexPath.row];
    } else {
        cellText = [self returnStringWithBase:CheckStation5 forIndex:indexPath.row];
    }
    
    cell.titleLabel.text = cellText;
    
    if ([self.selectedRowsArray containsObject:indexPath]) {
        cell.rowBackground.image = [UIImage imageNamed:@"row_checked.png"];
    } else {
        cell.rowBackground.image = [UIImage imageNamed:@"row_unchecked.png"];
    }

    [cell setAccessoryType:UITableViewCellAccessoryNone];
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    CheckMarkCell *selectedCell = (CheckMarkCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    
    BOOL checked = NO;
    if ([self.selectedRowsArray containsObject:indexPath]) {
        [self.selectedRowsArray removeObject:indexPath];
        [self.selectedRowsTitles removeObject:selectedCell.titleLabel.text];
        
        checked = NO;
    } else if ([self.selectedRowsArray count] == 3) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:AlertMessage2
                                                           message:@""
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil, nil];
        [alertView show];
        [alertView release];
        
    } else {
        [self.selectedRowsArray addObject:indexPath];
        [self.selectedRowsTitles addObject:selectedCell.titleLabel.text];

        checked = YES;
    }
    
    selectedCell.rowBackground.image = [UIImage imageNamed: checked ? @"row_checked.png" : @"row_unchecked.png"];
    
    if ([self.selectedRowsArray count]) {
        [self showSearchButton];
    } else {
        [self hideSearchButton];
    }
}

- (NSString *)returnStringWithBase:(NSString *)baseString forIndex:(int)index {
    self.optionsArray = [baseString componentsSeparatedByString:@";"];
    NSString *result = [self.optionsArray objectAtIndex:index];
    return result;
}

- (IBAction)search:(id)sender {
    NSLog(@"Search button pressed");
    
    //save selected datas to a sigletone
    NSString *info = [[SearchInfo sharedInstance] stringFromArray:self.selectedRowsTitles];
    [[SearchInfo sharedInstance] setStationControllerInfo:info];
    
    if (self.isQuickSearch) {
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
    } else {
        CityDetailsViewController *detailsController = [[CityDetailsViewController alloc] initWithNibName:@"CityDetailsViewController" bundle:nil];
        [self.navigationController pushViewController:detailsController animated:YES];
        [detailsController release];
        
    }
}

- (void)showSearchButton {
    [self checkScreenSizeButtonOn];
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationCurveEaseInOut
                     animations:^{self.searchButton.alpha = 1.0;}
                     completion:nil];

}

- (void)hideSearchButton {
    [self checkScreenSizeButtonOff];
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationCurveEaseInOut
                     animations:^{self.searchButton.alpha = 0.0;}
                     completion:nil];

}

#pragma mark SearchButton appearance
- (void)checkScreenSizeButtonOn {
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        [self.tableView setFrame:CGRectMake(0.0, 0.0, 320.0, 416.0)];
    } else {
        // code for 3.5-inch screen
        [self.tableView setFrame:CGRectMake(0.0, 0.0, 320.0, 323.0)];
        [self.searchButton setFrame:CGRectMake(0.0, 323.0, 320.0, 44.0)];
    }
}

- (void)checkScreenSizeButtonOff {
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        [self.tableView setFrame:CGRectMake(0.0, 0.0, 320.0, 455.0)];
    } else {
        // code for 3.5-inch screen
        [self.tableView setFrame:CGRectMake(0.0, 0.0, 320.0, 367.0)];
    }
}

- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
