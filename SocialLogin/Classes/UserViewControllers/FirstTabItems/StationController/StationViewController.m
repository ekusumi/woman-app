//
//  StationViewController.m
//  SocialLogin
//
//  Created by sebastian.achim on 7/13/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import "StationViewController.h"
#import "StationSelectedViewController.h"
#import "SearchInfo.h"

@interface StationViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong)IBOutlet UITableView *tableView;

@end

@implementation StationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = StationTabTitle;

    }
    return self;
}

-(id)initwithBool:(BOOL)isQuickSearchController {
    self = [super init];
    if (self) {
        // Custom initialization
        
        self.isQuickSearchController = isQuickSearchController;
    }
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar.png"] forBarMetrics:UIBarMetricsDefault];

    //back button from details view to hotOffers view
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 54.0, 35.0)];
    [backButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    [backButton setTitle:NSLocalizedString(@"", nil) forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    
    _backBarBtn = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton release];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationItem setLeftBarButtonItem:_backBarBtn animated:YES];

    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [Stations componentsSeparatedByString:@";"].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d", indexPath.row];
    
    // Create the coupon cell
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    

    NSString *cellText;
    
    cellText = [self returnStringWithBase:Stations forIndex:indexPath.row];
    cell.textLabel.text = cellText;
    
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.isQuickSearchController) {
        StationSelectedViewController *stationController = [[StationSelectedViewController alloc] initWithBool:self.isQuickSearchController];
        [stationController setCellIndex:indexPath.row];
        [self.navigationController pushViewController:stationController animated:YES];
        [stationController release];
        
    } else {
        StationSelectedViewController *stationController = [[StationSelectedViewController alloc] init];
        [stationController setCellIndex:indexPath.row];
        [self.navigationController pushViewController:stationController animated:YES];
        [stationController release];
    }
    
    [[SearchInfo sharedInstance] setStation:[self returnStringWithBase:Stations forIndex:indexPath.row]];
}

- (NSString *)returnStringWithBase:(NSString *)baseString forIndex:(int)index {
    self.optionsArray = [baseString componentsSeparatedByString:@";"];
    NSString *result = [self.optionsArray objectAtIndex:index];
    return result;
}

- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
