//
//  StationSelectedViewController.h
//  SocialLogin
//
//  Created by sebastian.achim on 7/18/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StationSelectedViewController : UIViewController

@property (nonatomic, retain) NSArray           *optionsArray;
@property (nonatomic, retain) NSString          *stationName;
@property (nonatomic, retain) NSArray           *cellIndexArray;
@property (nonatomic, assign) int               cellIndex;

@property (nonatomic,assign)BOOL isQuickSearchController;
@property (nonatomic, retain) UIBarButtonItem       *backBarBtn;

-(id)initWithBool:(BOOL)isQuickSearchController;

@end
