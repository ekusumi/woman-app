//
//  StationCheckMarkViewController.h
//  SocialLogin
//
//  Created by sebastian.achim on 7/18/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StationCheckMarkViewController : UIViewController

@property (nonatomic, retain) NSString              *navTitle;

@property (nonatomic, retain) NSMutableArray        *selectedRowsArray;
@property (nonatomic, retain) NSMutableArray        *selectedRowsTitles;

@property (nonatomic, retain) NSArray               *optionsArray;

@property (nonatomic, strong)IBOutlet UIButton      *searchButton;

@property (nonatomic, assign) int                   cellIndex;

@property (nonatomic, assign) BOOL                  isQuickSearch;

@property (nonatomic, retain) UIBarButtonItem       *backBarBtn;

-(id)initWithBool:(BOOL)isQuickSearch;

- (IBAction)search:(id)sender;

@end
