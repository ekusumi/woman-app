//
//  StationSelectedViewController.m
//  SocialLogin
//
//  Created by sebastian.achim on 7/18/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import "StationSelectedViewController.h"
#import "StationCheckMarkViewController.h"

@interface StationSelectedViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong)IBOutlet UITableView *tableView;

@end

@implementation StationSelectedViewController

- (id)init {
    self = [super init];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

-(id)initWithBool:(BOOL)isQuickSearchController {
    self = [super init];
    if (self) {
        // Custom initialization
        
        self.isQuickSearchController = isQuickSearchController;
    }
    return self;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar.png"] forBarMetrics:UIBarMetricsDefault];

    //back button from details view to hotOffers view
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 54.0, 35.0)];
    [backButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    [backButton setTitle:NSLocalizedString(@"", nil) forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    
    _backBarBtn = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton release];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationItem setLeftBarButtonItem:_backBarBtn animated:YES];
    
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    
    //set the navigation bars title
    self.title = [[Stations componentsSeparatedByString:@";"] objectAtIndex:self.cellIndex];
    
    //keep all entries in array
    self.cellIndexArray = [Stations componentsSeparatedByString:@";"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger numberOfRows;
    
    if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:0]]){
        numberOfRows = [StationsEntry1 componentsSeparatedByString:@";"].count;
    } else if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:1]]){
        numberOfRows = [StationsEntry2 componentsSeparatedByString:@";"].count;
    } else if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:2]]){
        numberOfRows = [StationsEntry3 componentsSeparatedByString:@";"].count;
    } else if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:3]]){
        numberOfRows = [StationsEntry4 componentsSeparatedByString:@";"].count;
    } else if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:4]]){
        numberOfRows = [StationsEntry5 componentsSeparatedByString:@";"].count;
    } else if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:5]]){
        numberOfRows = [StationsEntry6 componentsSeparatedByString:@";"].count;
    } else if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:6]]){
        numberOfRows = [StationsEntry7 componentsSeparatedByString:@";"].count;
    } else {
        numberOfRows = [StationsEntry8 componentsSeparatedByString:@";"].count;
    }
    
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d", indexPath.row];
    
    // Create the coupon cell
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    
    NSString *cellText;
    if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:0]]){
        cellText = [self returnStringWithBase:StationsEntry1 forIndex:indexPath.row];
    } else if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:1]]){
        cellText = [self returnStringWithBase:StationsEntry2 forIndex:indexPath.row];
    } else if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:2]]){
        cellText = [self returnStringWithBase:StationsEntry3 forIndex:indexPath.row];
    } else if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:3]]){
        cellText = [self returnStringWithBase:StationsEntry4 forIndex:indexPath.row];
    } else if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:4]]){
        cellText = [self returnStringWithBase:StationsEntry5 forIndex:indexPath.row];
    } else if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:5]]){
        cellText = [self returnStringWithBase:StationsEntry6 forIndex:indexPath.row];
    } else if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:6]]){
        cellText = [self returnStringWithBase:StationsEntry7 forIndex:indexPath.row];
    } else {
        cellText = [self returnStringWithBase:StationsEntry8 forIndex:indexPath.row];
    }
    
    cell.textLabel.text = cellText;
    
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.isQuickSearchController) {
        StationCheckMarkViewController *cityController = [[StationCheckMarkViewController alloc] initWithBool:self.isQuickSearchController];
        [cityController setCellIndex:indexPath.row];
        [self.navigationController pushViewController:cityController animated:YES];
        [cityController release];
    } else {
        StationCheckMarkViewController *stationController = [[StationCheckMarkViewController alloc] init];
        [stationController setCellIndex:indexPath.row];
        NSLog(@"--------INDEXPATH.ROW VALUE: %d--------",indexPath.row);
        [self.navigationController pushViewController:stationController animated:YES];
        [stationController release];
    }
}

- (NSString *)returnStringWithBase:(NSString *)baseString forIndex:(int)index {
    self.optionsArray = [baseString componentsSeparatedByString:@";"];
    NSString *result = [self.optionsArray objectAtIndex:index];
    return result;
}

- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
