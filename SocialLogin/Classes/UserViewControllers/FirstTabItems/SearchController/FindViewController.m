//
//  FindViewController.m
//  SocialLogin
//
//  Created by sebastian.achim on 7/13/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import "FindViewController.h"
#import "SelectionViewController.h"
#import "LocationViewController.h"
#import "StationViewController.h"
#import "ChatViewController.h"
#import "SearchInfo.h"
#import "CityDetailsViewController.h"

@interface FindViewController () <UITextFieldDelegate, UIScrollViewDelegate>

@property (nonatomic, retain)IBOutlet UIScrollView      *scrollView;

@property (nonatomic, retain)IBOutlet UITextField       *textField1;
@property (nonatomic, retain)IBOutlet UIButton          *buttonField1;

@property (nonatomic, retain)IBOutlet UITextField       *textField2;
@property (nonatomic, retain)IBOutlet UIButton          *buttonField2;

@property (nonatomic, retain)IBOutlet UITextField       *textField3;
@property (nonatomic, retain)IBOutlet UIButton          *buttonField3;

@property (nonatomic, retain)IBOutlet UITextField       *textField4;
@property (nonatomic, retain)IBOutlet UIButton          *buttonField4;

@property (nonatomic, retain)IBOutlet UIButton          *searchButton;

-(IBAction)selectFirstTextfield:(id)sender;
-(IBAction)selectSecondTextfield:(id)sender;
-(IBAction)selectThirdTextfield:(id)sender;
-(IBAction)selectFourthTextfield:(id)sender;

-(IBAction)searchPressed:(id)sender;

@end

@implementation FindViewController

-(id)init {
    self = [super init];
    if (self) {
        // Custom initialization
        
        self.title = SearchControllerTitle;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar.png"] forBarMetrics:UIBarMetricsDefault];

    //back button from details view to hotOffers view
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 54.0, 35.0)];
    [backButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    [backButton setTitle:NSLocalizedString(@"", nil) forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    
    _backBarBtn = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton release];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationItem setLeftBarButtonItem:_backBarBtn animated:YES];

    //set scrollView content size
    [self.scrollView setContentSize:CGSizeMake(0,500)];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
        
    //populate every field with selected options
    [self.textField1 setText:[SearchInfo sharedInstance].selectionControllerInfo];
    [self.textField2 setText:[SearchInfo sharedInstance].stationControllerInfo];
    [self.textField3 setText:[SearchInfo sharedInstance].locationControllerInfo];
    [self.textField4 setText:[SearchInfo sharedInstance].chatControllerInfo];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Private Methods

-(IBAction)selectFirstTextfield:(id)sender {
    NSLog(@"Textfield 1 pressed");
    
    BOOL isQuickSearchView = YES;
    SelectionViewController *selectionController = [[SelectionViewController alloc]initwithBool:isQuickSearchView];
    [self.navigationController pushViewController:selectionController animated:YES];
    [selectionController release];
}

-(IBAction)selectSecondTextfield:(id)sender {
    NSLog(@"Textfield 2 pressed");
    
    BOOL isQuickSearchView = YES;
    StationViewController *stationController = [[StationViewController alloc]initwithBool:isQuickSearchView];
    [self.navigationController pushViewController:stationController animated:YES];
    [stationController release];
}

-(IBAction)selectThirdTextfield:(id)sender {
    NSLog(@"Textfield 3 pressed");
    
    BOOL isQuickSearchView = YES;
    LocationViewController *locationController = [[LocationViewController alloc]initwithBool:isQuickSearchView];
    [self.navigationController pushViewController:locationController animated:YES];
    [locationController release];
}

-(IBAction)selectFourthTextfield:(id)sender {
    NSLog(@"Textfield 4 pressed");
    
    BOOL isQuickSearchView = YES;
    ChatViewController *chatController = [[ChatViewController alloc]initwithBool:isQuickSearchView];
    [self.navigationController pushViewController:chatController animated:YES];
    [chatController release];
}

-(IBAction)searchPressed:(id)sender {    

    if (![NSString isNilOrEmpty:self.textField1.text] ||
        ![NSString isNilOrEmpty:self.textField2.text] ||
        ![NSString isNilOrEmpty:self.textField3.text] ||
        ![NSString isNilOrEmpty:self.textField4.text]) {
        
        CityDetailsViewController *detailsController = [[CityDetailsViewController alloc] initWithNibName:@"CityDetailsViewController" bundle:nil];
        [self.navigationController pushViewController:detailsController animated:YES];
        [detailsController release];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:AlertMessage3
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
}

// -----------------------------------------------------------------------------
// UITextFieldDelegate methods
// -----------------------------------------------------------------------------
#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}

- (void)goBack:(id)sender {
    [[SearchInfo sharedInstance]clearAllInfo];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
