//
//  FindViewController.h
//  SocialLogin
//
//  Created by sebastian.achim on 7/13/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FindViewController : UIViewController

@property (nonatomic, retain) NSArray * seletedOptionsArray;
@property (nonatomic, retain) UIBarButtonItem       *backBarBtn;

@end
