//
//  CustomMapViewController.m
//  SocialLogin
//
//  Created by Razvan on 7/18/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import "CustomMapViewController.h"
#import "ListViewController.h"

@interface CustomMapViewController () {
    BOOL _isMenuVisible;
}

@end

@implementation CustomMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar.png"] forBarMetrics:UIBarMetricsDefault];
    
    //back button from details view to hotOffers view
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 54.0, 35.0)];
    [backButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    [backButton setTitle:NSLocalizedString(@"", nil) forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    
    _backBarBtn = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton release];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationItem setLeftBarButtonItem:_backBarBtn animated:YES];

    self.center = [[UIView alloc]initWithFrame:CGRectMake(100.0, 6.0, 105.0, 33.0)];
    [self.center setBackgroundColor:[UIColor clearColor]];
    
    // Need to do all this initialisation before calling base classes
    UISegmentedControl * segmentedControl = [[UISegmentedControl alloc] initWithItems: [NSArray arrayWithObjects: @"C1", @"C2", nil]]; // 1
    
    [segmentedControl addTarget:self action:@selector(middleSegmentChanged:) forControlEvents:UIControlEventValueChanged]; // 2
    segmentedControl.frame = CGRectMake(15.0, 0, 80.0, 29.0); // 3
    segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar; // 4
    segmentedControl.selectedSegmentIndex = 0; // 5
    segmentedControl.tintColor = APP_TEXT_COLOR_PINK;
    [self.center addSubview:segmentedControl];
    
    
    self.right = [[UIView alloc]initWithFrame:CGRectMake(215.0, 6.0, 100.0, 33.0)];
    [self.right setBackgroundColor:[UIColor clearColor]];
    
    // Need to do all this initialisation before calling base classes
    UISegmentedControl * rightSegmentedControl = [[UISegmentedControl alloc] initWithItems: [NSArray arrayWithObjects: @"R1", @"R2", nil]]; // 1
    
    [rightSegmentedControl addTarget:self action:@selector(rightSegmentChanged:) forControlEvents:UIControlEventValueChanged]; // 2
    rightSegmentedControl.frame = CGRectMake(15.0, 0, 80.0, 29.0); // 3
    rightSegmentedControl.segmentedControlStyle = UISegmentedControlStyleBar; // 4
    rightSegmentedControl.selectedSegmentIndex = 0; // 5
    rightSegmentedControl.tintColor = APP_TEXT_COLOR_PINK;
    [self.right addSubview:rightSegmentedControl];
    
    [self.navigationController.navigationBar addSubview:self.center];
    [self.navigationController.navigationBar addSubview:self.right];

    [segmentedControl release];
    [self.center release];
    [rightSegmentedControl release];
    [self.right release];
    
    _isMenuVisible = YES;
    
    [self.mapView setShowsUserLocation:YES];
    
    self.mapContainer.hidden = NO;
    self.detailsContainer.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [self.center setHidden:NO];
    [self.right setHidden:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    
    [self.center setHidden:YES];
    [self.right setHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_mapView release];
    [_toggleMenuBtn release];
    [_menuView release];
    [_mapContainer release];
    [_detailsContainer release];
    [super dealloc];
}

- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 2000000, 2000000);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    
    // Add an annotation
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = userLocation.coordinate;
    point.title = @"Current Location";
    
    [self.mapView addAnnotation:point];
}

- (MKAnnotationView *) mapView: (MKMapView *) mapView viewForAnnotation: (id<MKAnnotation>) annotation {
    if (annotation == mapView.userLocation) {
        return nil;
    }
    
    MKAnnotationView *pinView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"pinView"];
    if (!pinView) {
        pinView = [[[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"pinView"] autorelease];
    } else {
        pinView.annotation = annotation;
    }
    
    pinView.canShowCallout = YES;
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    pinView.rightCalloutAccessoryView = rightButton;
    
    return pinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    ListViewController *listController = [[ListViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
    [self.navigationController pushViewController:listController animated:YES];
    self.navigationController.navigationBar.hidden = NO;
    [listController release];
}

- (IBAction)toggleMenu:(UIButton *)sender {
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.menuView.frame;
        frame.origin.x = _isMenuVisible? 290.0 : 55.0;
        self.menuView.frame = frame;
        [sender setImage:_isMenuVisible? [UIImage imageNamed:@"icon_left"] : [UIImage imageNamed:@"icon_right"] forState:UIControlStateNormal];
        _isMenuVisible = !_isMenuVisible;
    }];
}

- (void)middleSegmentChanged:(id)sender {
    UISegmentedControl *control = (UISegmentedControl *)sender;

    if (control.selectedSegmentIndex == 0) {
        self.mapContainer.hidden = NO;
        self.detailsContainer.hidden = YES;
    } else {
        self.mapContainer.hidden = YES;
        self.detailsContainer.hidden = NO;
    }
}

- (void)rightSegmentChanged:(id)sender {
    UISegmentedControl *control = (UISegmentedControl *)sender;
    if (control.selectedSegmentIndex == 0) {
        
    } else {
        
    }
}

@end
