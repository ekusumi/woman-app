//
//  ListViewController.m
//  SocialLogin
//
//  Created by Razvan on 7/22/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import "ListViewController.h"

@interface ListViewController ()

@end

@implementation ListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar.png"] forBarMetrics:UIBarMetricsDefault];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//=======================================================================
// UITableViewDataSource methods
//=======================================================================
#pragma mark - UITableViewDataSource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = [NSString stringWithFormat:@"Cell%d", indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:indexPath.section ==0? UITableViewCellStyleValue2 : UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
    }
    
    if ([indexPath section] == 0) {
        cell.textLabel.text = @"Left text";
        cell.detailTextLabel.text = @"(Right text)";
    } else {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.textLabel.text = [NSString stringWithFormat:@"Section %d", indexPath.section];
        [cell.imageView setImage:[UIImage imageNamed:@"icon_user.png"]];
    }

    return cell;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

//=======================================================================
// UITableViewDelegate methods
//=======================================================================
#pragma mark - UITableViewDelegate methods
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *title;
    if (section == 1) {
        title = @"Section 1 title";
    } else {
        title = nil;
    }
    
    return title;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 44.0;
    
    if (indexPath.section == 0) {
        height = 60.0;
    }
    
    return height;
}

@end
