//
//  CustomMapViewController.h
//  SocialLogin
//
//  Created by Razvan on 7/18/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface CustomMapViewController : UIViewController <MKMapViewDelegate>

@property (retain, nonatomic) IBOutlet MKMapView *mapView;
@property (retain, nonatomic) IBOutlet UIButton *toggleMenuBtn;
@property (retain, nonatomic) IBOutlet UIView *menuView;
@property (retain, nonatomic) IBOutlet UIView *mapContainer;
@property (retain, nonatomic) IBOutlet UIView *detailsContainer;

@property (retain, nonatomic)  UIView *center;
@property (retain, nonatomic)  UIView *right;

@property (nonatomic, retain) UIBarButtonItem       *backBarBtn;


@end
