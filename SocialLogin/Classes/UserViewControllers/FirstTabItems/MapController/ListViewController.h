//
//  ListViewController.h
//  SocialLogin
//
//  Created by Razvan on 7/22/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
