//
//  LocationViewController.h
//  SocialLogin
//
//  Created by sebastian.achim on 7/13/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationViewController : UIViewController

@property (nonatomic,assign)BOOL isQuickSearchController;
@property (nonatomic, retain) UIBarButtonItem       *backBarBtn;

-(id)initwithBool:(BOOL)isQuickSearchController;

@end
