//
//  CityViewController.m
//  SocialLogin
//
//  Created by sebastian.achim on 7/15/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import "CityViewController.h"
#import "CityDetailsViewController.h"
#import "FindViewController.h"
#import "SearchInfo.h"

@interface CityViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong)IBOutlet UITableView   *tableView;
@property (nonatomic, strong)IBOutlet UIButton      *searchButton;


- (IBAction)search:(id)sender;

@end

@implementation CityViewController

- (id)init {
    self = [super init];
    if (self) {
        // Custom initialization
        self.selectedRowsArray = [NSMutableArray array];
        self.selectedRowsTitles = [NSMutableArray array];
    }
    return self;
}

-(id)initWithBool:(BOOL)isQuickSearch {
    self = [super init];
    if (self) {
        // Custom initialization
        self.selectedRowsArray = [NSMutableArray array];
        self.selectedRowsTitles = [NSMutableArray array];
        self.isQuickSearch = isQuickSearch;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar.png"] forBarMetrics:UIBarMetricsDefault];

    //back button from details view to hotOffers view
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 54.0, 35.0)];
    [backButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    [backButton setTitle:NSLocalizedString(@"", nil) forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    
    _backBarBtn = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton release];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationItem setLeftBarButtonItem:_backBarBtn animated:YES];

    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    
    //set the navigation bars title
    self.title = [[PrefectureListEntry componentsSeparatedByString:@";"] objectAtIndex:self.cellIndex];
    
    //keep all entries in array
    self.cellIndexArray = [PrefectureListEntry componentsSeparatedByString:@";"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger numberOfRows;
    
    if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:0]]){
        numberOfRows = [Tokyo componentsSeparatedByString:@";"].count;
    } else if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:1]]){
        numberOfRows = [Kanagawa componentsSeparatedByString:@";"].count;
    } else {
        numberOfRows = [Saitama componentsSeparatedByString:@";"].count;
    }
    
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CheckMarkCell *cell = [tableView dequeueReusableCellWithIdentifier:@"checkmarkCell"];
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CheckMarkCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    NSString *cellText;
    if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:0]]){
        cellText = [self returnStringWithBase:Tokyo forIndex:indexPath.row];
    } else if([self.title isEqualToString:[self.cellIndexArray objectAtIndex:1]]){
        cellText = [self returnStringWithBase:Kanagawa forIndex:indexPath.row];
    } else {
        cellText = [self returnStringWithBase:Saitama forIndex:indexPath.row];
    }
    
    cell.titleLabel.text = cellText;
    
    if ([self.selectedRowsArray containsObject:indexPath]) {
        cell.rowBackground.image = [UIImage imageNamed:@"row_checked.png"];
    } else {
        cell.rowBackground.image = [UIImage imageNamed:@"row_unchecked.png"];
    }
    
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (NSString *)returnStringWithBase:(NSString *)baseString forIndex:(int)index {
    self.optionsArray = [baseString componentsSeparatedByString:@";"];
    NSString *result = [self.optionsArray objectAtIndex:index];
    return result;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat alpha = 0.0;
    if (indexPath.row == 0) {
        BOOL checked;
        if ([self.selectedRowsArray count] == [self.optionsArray count]) {
            [self.selectedRowsArray removeAllObjects];
            [self.selectedRowsTitles removeAllObjects];
            checked = NO;
            
            alpha = 0.0;
            
            //check the size of the screen than hide button and resize tableview
            [self checkScreenSizeButtonOff];
            
            for (int i=0; i<[self.tableView numberOfRowsInSection:0]; i++) {
                NSIndexPath *allIndexPaths = [NSIndexPath indexPathForRow:i inSection:0];
                CheckMarkCell *selectedCell = (CheckMarkCell*)[self.tableView cellForRowAtIndexPath:allIndexPaths];
                selectedCell.rowBackground.image = [UIImage imageNamed:@"row_unchecked.png"];
            }
        } else {
            [self.selectedRowsArray removeAllObjects];
            [self.selectedRowsTitles removeAllObjects];
            for (int i=0; i<[self.tableView numberOfRowsInSection:0]; i++) {
                NSIndexPath *allIndexPaths = [NSIndexPath indexPathForRow:i inSection:0];
                [self.selectedRowsArray addObject:allIndexPaths];
            }
            
            //get all row titles without the first row ("select all") 
            NSArray *allSelectedTitles = [NSArray arrayWithArray:self.optionsArray];
            [self.selectedRowsTitles addObjectsFromArray:allSelectedTitles];
            [self.selectedRowsTitles removeObjectAtIndex:0];
            
            alpha = 1.0;
            
            //check the size of the screen than show button and resize tableview
            [self checkScreenSizeButtonOn];
            
            checked = YES;
            
            for (NSIndexPath *indexPath  in self.selectedRowsArray) {
                CheckMarkCell *selectedCell = (CheckMarkCell*)[self.tableView cellForRowAtIndexPath:indexPath];
                selectedCell.rowBackground.image = [UIImage imageNamed:@"row_checked.png"];
            }
        }
        
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options: UIViewAnimationCurveEaseInOut
                         animations:^{self.searchButton.alpha = alpha;}
                         completion:nil];
    } else {
        if ([self.selectedRowsArray count] == [self.optionsArray count]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:AlertMessage
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            
        } else {
            CheckMarkCell *selectedCell = (CheckMarkCell*)[self.tableView cellForRowAtIndexPath:indexPath];
            
            BOOL checked;
            if ([self.selectedRowsArray containsObject:indexPath]) {
                [self.selectedRowsArray removeObject:indexPath];
                [self.selectedRowsTitles removeObject:selectedCell.titleLabel.text];
                checked = NO;
            } else {
                [self.selectedRowsArray addObject:indexPath];
                [self.selectedRowsTitles addObject:selectedCell.titleLabel.text];
                checked = YES;
            }
            
//            NSLog(@"selectedRowsTitles %@", self.selectedRowsTitles);
            
            selectedCell.rowBackground.image = [UIImage imageNamed: checked ? @"row_checked.png" : @"row_unchecked.png"];
            
            if ([self.selectedRowsArray count]) {
                alpha = 1.0;
                //check the size of the screen than show button and resize tableview
                [self checkScreenSizeButtonOn];
            } else {
                alpha = 0.0;
                //check the size of the screen than hide button and resize tableview
                [self checkScreenSizeButtonOff];
            }
            
            [UIView animateWithDuration:0.5
                                  delay:0.0
                                options: UIViewAnimationCurveEaseInOut
                             animations:^{self.searchButton.alpha = alpha;}
                             completion:nil];
        }
    }
}

#pragma mark Private Button Actions
- (IBAction)search:(id)sender {
    NSLog(@"Search button pressed");
    
    NSString *info = [[SearchInfo sharedInstance] stringFromArray:self.selectedRowsTitles];
    [[SearchInfo sharedInstance] setLocationControllerInfo:info];
    
    if (self.isQuickSearch) {
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
    } else {
        CityDetailsViewController *detailsController = [[CityDetailsViewController alloc] initWithNibName:@"CityDetailsViewController" bundle:nil];
        [self.navigationController pushViewController:detailsController animated:YES];
        [detailsController release];
        
    }
}

#pragma mark SearchButton appearance
- (void)checkScreenSizeButtonOn {
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        [self.tableView setFrame:CGRectMake(0.0, 0.0, 320.0, 416.0)];
    } else {
        // code for 3.5-inch screen
        [self.tableView setFrame:CGRectMake(0.0, 0.0, 320.0, 323.0)];
        [self.searchButton setFrame:CGRectMake(0.0, 323.0, 320.0, 44.0)];
    }
}

- (void)checkScreenSizeButtonOff {
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        [self.tableView setFrame:CGRectMake(0.0, 0.0, 320.0, 455.0)];
    } else {
        // code for 3.5-inch screen
        [self.tableView setFrame:CGRectMake(0.0, 0.0, 320.0, 367.0)];
    }
}

- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end

