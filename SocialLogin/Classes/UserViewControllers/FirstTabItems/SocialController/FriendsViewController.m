//
//  FriendsViewController.m
//  suregift
//
//  Created by Matteo Gobbi on 31/12/12.
//  Copyright (c) 2012 Matteo Gobbi. All rights reserved.
//

#import "FriendsViewController.h"
#import "FriendsCell.h"
#import "FriendsBanners.h"
#import "LocationViewController.h"
#import "StationViewController.h"
#import "ChatViewController.h"
#import "FindViewController.h"
#import "CustomMapViewController.h"
#import "SelectionViewController.h"
#import "CityDetailsViewController.h"
#import "SearchInfo.h"

@interface FriendsViewController ()

@property (nonatomic, retain)IBOutlet UIButton           *mapButton;
@property (nonatomic, retain)IBOutlet UIButton           *homeButton;
@property (nonatomic, retain)IBOutlet UIButton           *mapPinButton;
@property (nonatomic, retain)IBOutlet UIButton           *stationButton;
@property (nonatomic, retain)IBOutlet UIButton           *chatButton;
@property (nonatomic, retain)IBOutlet UIButton           *searchButton;
@property (nonatomic, retain)IBOutlet UIButton           *prButton;

@property (nonatomic, retain) IBOutlet UIImageView       *backgroundView;

-(IBAction)menuButtonsPressed:(id)sender;
-(IBAction)prButtonPressed:(id)sender;
-(void)logout:(id)sender;

-(void)setDefaultValue:(NSString *)value forKey:(NSString *)key;

@end

@implementation FriendsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        [self setTitle:@"Friends"];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    //set navigation bar title
    self.navigationItem.title = MenuViewTitle;

    //add custom back button to the navigation bar
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 54.0, 35.0)];
    [backButton addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"logout_btn.png"] forState:UIControlStateNormal];
    [backButton setTitle:NSLocalizedString(@"", nil) forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    
    _backBarBtn = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationItem setLeftBarButtonItem:_backBarBtn animated:YES];
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        [self.backgroundView setFrame:CGRectMake(0.0, -10.0, 320.0, 536.0)];
        [self.prButton setFrame:CGRectMake(0.0, 463.0, 320.0, 41.0)];
    }
    
    //set bottom button title
    [self.prButton setTitle:UnderMenuButtonTitle forState:UIControlStateNormal];
    
    [scrollView setScrollEnabled:YES];
    [scrollView setContentSize:CGSizeMake(320.0, 800.0)];
    
    [self getImages];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
//    [self.parentViewController setTitle:FriendsTabTitle];
    
    [[SearchInfo sharedInstance] clearAllInfo];
    
}

#pragma mark UIButton Actions
-(IBAction)prButtonPressed:(id)sender {
    CityDetailsViewController *detailsController = [[CityDetailsViewController alloc] initWithNibName:@"CityDetailsViewController" bundle:nil];
    [self.navigationController pushViewController:detailsController animated:YES];
    [detailsController release];
    
    self.navigationController.navigationBar.hidden = NO;
}

-(IBAction)menuButtonsPressed:(id)sender {
    if ((UIButton*)sender == self.mapButton) {
        CustomMapViewController *mapController = [[CustomMapViewController alloc] initWithNibName:@"CustomMapViewController" bundle:nil];
        [self.navigationController pushViewController:mapController animated:YES];
        [mapController release];
        
        self.navigationController.navigationBar.hidden = NO;
    } else if ((UIButton*)sender == self.homeButton) {
        SelectionViewController *locationController = [[SelectionViewController alloc]init];
        [self.navigationController pushViewController:locationController animated:YES];
        [locationController release];
        
        self.navigationController.navigationBar.hidden = NO;
        
    } else if ((UIButton*)sender == self.mapPinButton) {
        LocationViewController *locationController = [[LocationViewController alloc]init];
        [self.navigationController pushViewController:locationController animated:YES];
        [locationController release];
        
        self.navigationController.navigationBar.hidden = NO;
    } else if ((UIButton*)sender == self.stationButton) {
        StationViewController *stationController = [[StationViewController alloc]init];
        [self.navigationController pushViewController:stationController animated:YES];
        [stationController release];
        
        self.navigationController.navigationBar.hidden = NO;
    } else if ((UIButton*)sender == self.chatButton) {
        ChatViewController *chatController = [[ChatViewController alloc]init];
        [self.navigationController pushViewController:chatController animated:YES];
        [chatController release];
        
        self.navigationController.navigationBar.hidden = NO;
    } else if ((UIButton*)sender == self.searchButton) {
        FindViewController *findController = [[FindViewController alloc]init];
        [self.navigationController pushViewController:findController animated:YES];
        [findController release];
        
        self.navigationController.navigationBar.hidden = NO;
    }
    
}

- (void) getImages {
    
    NSArray *urls = @[@"http://182.48.52.97/tainyuapp/1.jpg",
                      @"http://182.48.52.97/tainyuapp/2.png",
                      @"http://182.48.52.97/tainyuapp/3.png",
                      @"http://182.48.52.97/tainyuapp/4.png",
                      @"http://182.48.52.97/tainyuapp/5.png"];
    NSArray *images = @[img1, img2, img3, img4, img5];
    NSArray *spinners = @[spin1, spin2, spin3, spin4, spin5];
    
    for(int i=0; i<[urls count]; i++) {
        [spinners[i] setHidden:NO];
        dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(concurrentQueue, ^{
            NSData *image = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:urls[i]]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [images[i] setImage:[UIImage imageWithData:image]];
                [spinners[i] setHidden:YES];
            });
        });
    }
}

-(void)logout:(id)sender {
    //Sloggo e cancello user default facebook
    if(FBSession.activeSession.isOpen)
        [FBSession.activeSession closeAndClearTokenInformation];
    
    //Cancello user default sessione etc.
    [self setDefaultValue:@"" forKey:USER_SESSION];
    [self setDefaultValue:@"" forKey:USER_FACEBOOK_LOGIN];
    [self setDefaultValue:@"" forKey:USER_IMG_PROFILE];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)setDefaultValue:(NSString *)value forKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:value forKey:key];
    [defaults synchronize];
}

#pragma mark UISearchBarDelegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)sBar {
    searchBar.showsCancelButton = YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)sBar {
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) sBar {
    [self.view endEditing:YES];
    searchBar.showsCancelButton = NO;
}

@end
