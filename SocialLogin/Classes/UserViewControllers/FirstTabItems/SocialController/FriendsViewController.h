//
//  FriendsViewController.h
//  suregift
//
//  Created by Matteo Gobbi on 31/12/12.
//  Copyright (c) 2012 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsViewController : UIViewController <UISearchBarDelegate> {
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIImageView *img1, *img2, *img3, *img4, *img5;
    IBOutlet UIActivityIndicatorView *spin1, *spin2, *spin3, *spin4, *spin5;
    IBOutlet UISearchBar *searchBar;
}

@property (nonatomic, retain) UIBarButtonItem       *backBarBtn;

@end
