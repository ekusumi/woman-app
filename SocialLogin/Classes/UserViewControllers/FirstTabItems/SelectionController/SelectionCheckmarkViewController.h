//
//  SelectionCheckmarkViewController.h
//  SocialLogin
//
//  Created by sebastian.achim on 7/22/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckMarkCell.h"

@interface SelectionCheckmarkViewController : UIViewController

@property (nonatomic, retain) NSMutableArray    *selectedRowsArray;
@property (nonatomic, retain) NSMutableArray    *selectedRowsTitles;

@property (nonatomic, retain) NSArray           *optionsArray;
@property (nonatomic, retain) NSArray           *cellIndexArray;

@property (nonatomic, retain) CheckMarkCell     *selectedCell;

@property (nonatomic, assign) int               cellIndex;

@property (nonatomic, assign) BOOL               isQuickSearch;

@property (nonatomic, retain) UIBarButtonItem       *backBarBtn;

-(id)initWithBool:(BOOL)isQuickSearch;

@end
