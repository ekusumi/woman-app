//
//  PointsDetailViewController.m
//  SocialLogin
//
//  Created by Me on 8/31/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import "PointsDetailViewController.h"
#import "SVProgressHUD.h"
#import "UserManager.h"
#import "DataManager.h"
#import "GenericWebViewController.h"

@interface PointsDetailViewController ()

@end

@implementation PointsDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 54.0, 35.0)];
    [backButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    [backButton setTitle:NSLocalizedString(@"", nil) forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    
    UIBarButtonItem *_backBarBtn = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationItem setLeftBarButtonItem:_backBarBtn animated:YES];
    self.navigationItem.title = @"Points";
    
    [lblTitle setText:_userInfo.title];
    [lblDeadline setText:_userInfo.deadline];
    
    btnApply.layer.cornerRadius = 10;
    imgInfo.layer.cornerRadius = 10;
    
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeGradient];
    
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(concurrentQueue, ^{
        NSData *image = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:_userInfo.imageURL]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [imgInfo setImage:[UIImage imageWithData:image]];
            [SVProgressHUD dismiss];
        });
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onApply:(id)sender {
    if([[UserManager sharedInstance] userLoggedIn]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Social Login" message:@"Would you like to apply?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [alert setTag:1];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Social Login" message:@"You will need an account in order to apply. Would you like to sign-up?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [alert setTag:2];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex!=alertView.cancelButtonIndex) {
        if(alertView.tag==1) {
            NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:@"id", @"3", @"token", @"asfdsfasdfa", @"1375", @"event_id", nil];
            [[DataManager sharedInstance] postRequestWithParameters:dict atPath:APPLY_URL inBlock:^(id results) {
                NSString *message = @"";
                if(results==nil) message = @"Failed!";
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Social Login" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert setTag:3];
                [alert show];
            }];
        } else if(alertView.tag==2) {
            GenericWebViewController *controller = [[GenericWebViewController alloc] initWithNibName:@"WebView" bundle:nil];
            controller.URLstring = SIGN_UP_URL;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

@end
