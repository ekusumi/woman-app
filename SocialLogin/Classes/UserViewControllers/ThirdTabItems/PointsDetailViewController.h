//
//  PointsDetailViewController.h
//  SocialLogin
//
//  Created by Me on 8/31/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "UserInfo.h"

@interface PointsDetailViewController : UIViewController {
    IBOutlet UIImageView *imgInfo;
    IBOutlet UILabel *lblTitle, *lblDeadline;
    IBOutlet UIButton *btnApply;
}

@property (nonatomic, retain) UserInfo *userInfo;

- (IBAction)onApply:(id)sender;

@end
