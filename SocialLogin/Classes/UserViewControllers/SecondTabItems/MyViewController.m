//
//  MyViewController.m
//  suregift
//
//  Created by Matteo Gobbi on 31/12/12.
//  Copyright (c) 2012 Matteo Gobbi. All rights reserved.
//

#import "MyViewController.h"
#import "MyTableViewCell.h"
#import "DataManager.h"
#import "UserManager.h"
#import "SVProgressHUD.h"
#import "UserInfo.h"
#import "GenericWebViewController.h"

@interface MyViewController ()

@end

@implementation MyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
//        [self setTitle:@"Me"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar.png"] forBarMetrics:UIBarMetricsDefault];
    
    _profileImageView.image = [UIImage imageWithData:[Utility decryptStringToData:[Utility getDefaultValueForKey:USER_IMG_PROFILE]]];
    self.navigationItem.title = @"Me";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeGradient];
    [self retrieveData];
    [self loadProfileImage];
}

//=======================================================================
// Data Handlers
//=======================================================================
#pragma mark - Data Handlers
- (void)retrieveData {
//    [self.userTableView setHidden:YES];
//    [self.pointsLabel setHidden:YES];
    
//    [self.infoArray removeAllObjects];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSString *userId     = [[UserManager sharedInstance] userId];
    NSString *userToken  = [[UserManager sharedInstance] userToken];
    
    [params setObject:!userId ? @"":userId forKey:@"id"];
    [params setObject:!userToken ? @"":userToken forKey:@"token"];
    
    [[DataManager sharedInstance] postRequestWithParameters:params
                                                     atPath:@"histories/index"
                                                    inBlock:^(id results){
                                                        NSLog(@"Results %@", results);
                                                        _infoArray = [[NSArray alloc] initWithArray:results];
                                                        if([_infoArray count]> 0) {
                                                            [_tableView reloadData];
                                                        } else {
                                                            [self showMessage];
                                                        }
                                                        [SVProgressHUD dismiss];
                                                    }];
}

- (void) showMessage {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Social Login" message:@"There is no searching history." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

//-----------------------------------------------------------------------
// UITableViewDataSource methods
//-----------------------------------------------------------------------
#pragma mark - UITableViewDataSource methods
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//	return [_infoArray count];
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [_infoArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myTableViewCell"];
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MyTableViewCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    
    NSDictionary *dictionary = [_infoArray objectAtIndex:indexPath.row];
    NSDictionary *history = [dictionary objectForKey:@"History"];
        
    [cell.titleLabel setText:[history objectForKey:@"title"]];
    return cell;
}

//-----------------------------------------------------------------------
// UITableViewDelegate methods
//-----------------------------------------------------------------------
#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
    GenericWebViewController *controller = [[GenericWebViewController alloc] initWithNibName:@"WebView" bundle:nil];
//    [controller setURLstring:[content objectForKey:@"authorurl"]];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void) loadProfileImage {
    UIImage *image = [[UserManager sharedInstance] profileImage];
    NSLog(@"data %@", UIImagePNGRepresentation(image));
    [self.profileImageView setImage:image];
}

- (void)dealloc {
    [_profileImageView release];
    [super dealloc];
}
@end
