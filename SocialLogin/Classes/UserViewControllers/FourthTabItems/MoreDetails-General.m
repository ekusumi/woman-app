//
//  MoreDetails-General.m
//  Emerald
//
//  Created by ColtBoys on 1/13/13.
//  Copyright (c) 2013 coltboy. All rights reserved.
//

#import "MoreDetails-General.h"

@interface MoreDetails_General ()

@end

@implementation MoreDetails_General
@synthesize infos;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar.png"] forBarMetrics:UIBarMetricsDefault];
    
    //back button from details view to hotOffers view
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 54.0, 35.0)];
    [backButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    [backButton setTitle:NSLocalizedString(@"", nil) forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    
    _backBarBtn = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton release];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationItem setLeftBarButtonItem:_backBarBtn animated:YES];


    lblTitle.text = [[self.infos componentsSeparatedByString:@"#"]lastObject];
    
    if ([[[self.infos componentsSeparatedByString:@"#"]objectAtIndex:0]isEqualToString:@"map"]) {
        theMap.hidden=NO;
        txtView.hidden=YES;
        CLLocationCoordinate2D location;
        location.latitude = [[[[[self.infos componentsSeparatedByString:@"#"]objectAtIndex:1]componentsSeparatedByString:@","]objectAtIndex:0]floatValue];
        location.longitude = [[[[[self.infos componentsSeparatedByString:@"#"]objectAtIndex:1]componentsSeparatedByString:@","]objectAtIndex:1]floatValue];
        // Add the annotation to our map view
        newAnnotation = [[MapViewAnnotation alloc] initWithTitle:[[self.infos componentsSeparatedByString:@"#"]objectAtIndex:2] andCoordinate:location];
        [theMap addAnnotation:newAnnotation];
    } else {
        theMap.hidden=YES;
        theMap.delegate=nil;
        [theMap removeFromSuperview];
        theMap=nil;
        
        txtView.hidden=NO;
        txtView.text = [[self.infos componentsSeparatedByString:@"#"]objectAtIndex:1];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark MapKit protocol
- (void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views {
	MKAnnotationView *annotationView = [views objectAtIndex:0];
	id <MKAnnotation> mp = [annotationView annotation];
	MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([mp coordinate], 1500, 1500);
	[mv setRegion:region animated:YES];
	[mv selectAnnotation:mp animated:YES];
}

@end
