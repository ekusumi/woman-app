//
//  LoginViewController.m
//  eventbook
//
//  Created by Matteo Gobbi on 01/12/11.
//  Copyright (c) 2011 Matteo Gobbi - Ingegnere Informatico libero professionista. All rights reserved.
//

#import "MasterViewController.h"
#import "JoinViewController.h"
#import "UserManager.h"
#import "CustomAlertView.h"
#import "Utility.h"
#import "SignupViewController.h"

@interface MasterViewController ()
    -(void)initializeView;
@end

@implementation MasterViewController

@synthesize btJoin, joinViewC;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}


-(void)initializeView {
        
    //Setto bottoni
    UIImage *buttonImageSign = [[UIImage imageNamed:@"login_btn.png"]
                                resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    [_btSignIn setBackgroundImage:buttonImageSign forState:UIControlStateNormal];
    [_btSignIn.titleLabel setFont:[UIFont fontWithName:APP_FONT size:15]];
    
    
    UIImage *buttonImageJoin = [[UIImage imageNamed:@"newAccount_button.png"]
                            resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];

    [btJoin setBackgroundImage:buttonImageJoin forState:UIControlStateNormal];
}

- (void)initializePresentation {
    int currentPresNr = [[UserManager sharedInstance] presentationAlertNr];
    if (![[UserManager sharedInstance] userLoggedIn] &&
        currentPresNr <= 3) {
        
        CustomAlertView *alert = [[CustomAlertView alloc] initWithTitle:nil
                                                                message:@"サービスの使い方を確認する？"
                                                               delegate:self
                                                      cancelButtonTitle:@"確認しない"
                                                      otherButtonTitles:@"確認する", nil];
        alert.tag = 1001;
        [alert show];
        [alert release];
        
        currentPresNr += 1;
        [[UserManager sharedInstance] setPresentationAlertNr:currentPresNr];
        [[UserManager sharedInstance] saveUserInfo];
    }
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar.png"] forBarMetrics:UIBarMetricsDefault];

    self.title = LoginViewTitle;
    
    [self initializePresentation];
    [self initializeView];
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        [self.backgroundView setFrame:CGRectMake(0.0, -10.0, 320.0, 536.0)];
        [self.btJoin setFrame:CGRectMake(0.0, 463.0, 320.0, 41.0)];
    } 
    
    //Colore testo placeholder etc - grigio scuro
    UIColor *color = [UIColor colorWithRed:180.0/255.0 green:180.0/255.0 blue:180.0/255.0 alpha:1.0];

    self.usernameTextfield = [[UITextField alloc] initWithFrame:CGRectMake(65.0, 58.0, 225.0, 30.0 )];
    self.usernameTextfield.adjustsFontSizeToFitWidth = YES;
    [self.usernameTextfield setBackgroundColor:[UIColor clearColor]];
    [self.usernameTextfield setFont:[UIFont fontWithName:APP_FONT size:16]];
    [self.usernameTextfield setTextColor:[UIColor colorWithRed:APP_TEXT_COLOR_GRAY green:APP_TEXT_COLOR_GRAY blue:APP_TEXT_COLOR_GRAY alpha:1.0]];
    //txt.placeholder = @"E-mail";
    self.usernameTextfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"E-mail" attributes:@{NSForegroundColorAttributeName: color}];
    self.usernameTextfield.keyboardType = UIKeyboardTypeDefault;
    self.usernameTextfield.returnKeyType = UIReturnKeyNext;
    self.usernameTextfield.autocorrectionType = UITextAutocorrectionTypeNo; // no auto correction support
    self.usernameTextfield.autocapitalizationType = UITextAutocapitalizationTypeNone; // no auto capitalization support
    self.usernameTextfield.textAlignment = NSTextAlignmentLeft;
    self.usernameTextfield.delegate = self;
    self.usernameTextfield.clearButtonMode = UITextFieldViewModeNever; // no clear 'x' button to the right
    [self.usernameTextfield setEnabled: YES];

    [self.view addSubview:self.usernameTextfield];
    [self.usernameTextfield release];
    
    self.passwordTextfield = [[UITextField alloc] initWithFrame:CGRectMake(65.0, 111.0, 225.0, 30.0 )];
    self.passwordTextfield.adjustsFontSizeToFitWidth = YES;
    [self.passwordTextfield setBackgroundColor:[UIColor clearColor]];
    [self.passwordTextfield setFont:[UIFont fontWithName:APP_FONT size:16]];
    [self.passwordTextfield setTextColor:[UIColor colorWithRed:APP_TEXT_COLOR_GRAY green:APP_TEXT_COLOR_GRAY blue:APP_TEXT_COLOR_GRAY alpha:1.0]];
    //txt.placeholder = @"Password";
    self.passwordTextfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    self.passwordTextfield.keyboardType = UIKeyboardTypeDefault;
    self.passwordTextfield.returnKeyType = UIReturnKeyDone;
    self.passwordTextfield.secureTextEntry = YES;
    self.passwordTextfield.autocorrectionType = UITextAutocorrectionTypeNo; // no auto correction support
    self.passwordTextfield.autocapitalizationType = UITextAutocapitalizationTypeNone; // no auto capitalization support
    self.passwordTextfield.textAlignment = NSTextAlignmentLeft;
    self.passwordTextfield.delegate = self;
    self.passwordTextfield.clearButtonMode = UITextFieldViewModeNever; // no clear 'x' button to the right
    [self.passwordTextfield setEnabled: YES];
    [self.view addSubview:self.passwordTextfield];
    [self.passwordTextfield release];
    
    if(![[Utility getSession] isEqualToString:@""]) {
        if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
            // Yes, so just open the session (this won't display any UX).
            //Questo mi rilogga con una nuova sessione
        } else {
            //Sfrutto la session che ho già
            [self goToCommunityWithPhotoController:[[Utility getDefaultValueForKey:USER_IMG_PROFILE] isEqualToString:@""]];
        }
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    chooseProfileImageViewController = nil;
    communityTabBarController = nil;
    myViewController = nil;
    friendsViewController = nil;
    loadView = nil;
    loginParser = nil;
    btLogin = nil;
    self.btJoin = nil;
    self.joinViewC = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self] == NSNotFound) {
        //Faccio logout
    }
    [super viewWillDisappear:animated];
}

#pragma mark -
#pragma mark - Text Field Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    NSInteger nextTag = theTextField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [theTextField.superview.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [theTextField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}


#pragma mark -
#pragma mark - My Methods

-(IBAction)login:(id)sender {
    [self.view endEditing:YES];
    
    [self loginWithSendConfirm:NO];
    
    
   // [self goToCommunity];
}


-(void)loginWithSendConfirm:(BOOL)send {
    [self setModeLoading:YES];
    
    NSString *email = self.usernameTextfield.text;
    NSString *password = self.passwordTextfield.text;
    
    email = [Utility encryptString:email];
    password = [Utility encryptString:password];
    NSString *device = [Utility encryptString:[Utility getDeviceAppId]];
    NSString *token = [Utility encryptString:[Utility getDeviceToken]];
    
    NSString *strSend = @"false";
    if(send) strSend = @"true";
    
    /*
    //Creo la stringa di inserimento
    NSString *str = [URL_SERVER stringByAppendingString:[NSString stringWithFormat:@"login.php?email=%@&password=%@&device=%@&token=%@&send_confirm=%@", email, password, device, token, [strSend stringByAddingPercentEscapesUsingEncoding: 4]]];
    */
    
    //POST
    NSString *str = [URL_SERVER stringByAppendingString:@"login.php"];
    
    //Start parser thread
    if(loginParser == nil) {
        loginParser = [[RequestParser alloc] init];
    }
    [loginParser setStrURL:str];
    [loginParser addPostValue:email forKey:@"email"];
    [loginParser addPostValue:password forKey:@"password"];
    [loginParser addPostValue:device forKey:@"device"];
    [loginParser addPostValue:token forKey:@"token"];
    [loginParser addPostValue:[strSend stringByAddingPercentEscapesUsingEncoding: 4] forKey:@"send_confirm"];
    loginParser.delegate = self;
    [loginParser start];
}


-(IBAction)join:(id)sender {
//    if(self.joinViewC == nil) {
//        self.joinViewC = [[JoinViewController alloc] initWithNibName:@"JoinViewController" bundle:[NSBundle mainBundle]];
//    }
//    
//    [self presentViewController:joinViewC animated:YES completion:nil];
    
    SignupViewController *signup = [[SignupViewController alloc] initWithNibName:@"SignupViewController" bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:signup];
    [self presentViewController:nav animated:YES completion:nil];
}


//Login parser end
-(void) notifyThreadEnd:(NSDictionary *)responseDict {
    NSLog(@"RESPONSE %@", responseDict);
    [self setModeLoading:NO];
    
    [[UserManager sharedInstance] setUserId:nil];
    [[UserManager sharedInstance] setUserToken:nil];
    [[UserManager sharedInstance] setUserLoggedIn:NO];
    [[UserManager sharedInstance] setProfileImage:[UIImage new]];
    [[UserManager sharedInstance] saveUserInfo];
    
    if([[responseDict valueForKey:ERROR_KEY] isEqualToString:ERROR_CONNECTION]) {
        CustomAlertView *alert = [[CustomAlertView alloc] initWithTitle:@"Connection Error" message:@"There was an error connecting to the server. Make sure you have an active internet connection or try again later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        return;
    }
    
    NSString *login = [responseDict valueForKey:@"login"];
    
    if([login isEqualToString:@"1"] || [login isEqualToString:@"4"]) {
        
        [[UserManager sharedInstance] setUserId:[responseDict valueForKey:@"id"]];
        [[UserManager sharedInstance] setUserToken:[responseDict valueForKey:@"token"]];
        
        NSData* data = [[responseDict valueForKey:@"img_profile"] dataUsingEncoding: [NSString defaultCStringEncoding]];
        [[UserManager sharedInstance] setProfileImage:[UIImage imageWithData:data]];
        [[UserManager sharedInstance] setUserLoggedIn:YES];
        [[UserManager sharedInstance] saveUserInfo];
        
        //Accesso avvenuto
        NSString *session = [responseDict valueForKey:@"session"];
        NSString *facebook = [responseDict valueForKey:@"facebook"];
        NSString *image = [responseDict valueForKey:@"img_profile"];
        
        session = [Utility decryptString:session];
        facebook = [Utility decryptString:facebook];
        
        if(![session isEqualToString:@""] && session != nil) {
            [Utility setDefaultValue:session forKey:USER_SESSION];
            [Utility setDefaultValue:facebook forKey:USER_FACEBOOK_LOGIN];
        }
        
        if(image) {
            [Utility setDefaultValue:image forKey:USER_IMG_PROFILE];
        }
        
        [self goToCommunityWithPhotoController:NO]; // [login isEqualToString:@"4"]
        
    } else if([login isEqualToString:@"-1"]) {
        //Appare l'alert
        CustomAlertView *alert = [[CustomAlertView alloc] initWithTitle:@"Server Error" message:@"Error connect to database." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
    } else if([login isEqualToString:@"0"]) {
        //Appare l'alert
        CustomAlertView *alert = [[CustomAlertView alloc] initWithTitle:@"SocialLogin" message:@"E-mail and/or password wrong!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
    } else if([login isEqualToString:@"3"]) {
        //Appare l'alert
        CustomAlertView *alert = [[CustomAlertView alloc] initWithTitle:@"SocialLogin" message:@"Confirm e-mail send!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
    } else if([login isEqualToString:@"2"]) {
        //Appare l'alert
        CustomAlertView *alert = [[CustomAlertView alloc] initWithTitle:@"SocialLogin" message:@"You don't have confirmed your email address, do you want receive confirm email again?" delegate:self cancelButtonTitle:@"No, thanks" otherButtonTitles:@"Yes",nil];
        alert.tag = 1;
        [alert show];
        [alert release];
    }
    
}

-(void)goToCommunityWithPhotoController:(BOOL)flag {
    
    //Azzero i campi user e pass
    self.usernameTextfield.text = @"";
    self.passwordTextfield.text = @"";
    
    [self initializeTabBar];
    
    if(flag) {
        //if(chooseProfileImageViewController == nil) {
            chooseProfileImageViewController = [[ChooseProfileImageViewController alloc] initWithNibName:@"ChooseProfileImageViewController" bundle:[NSBundle mainBundle]];
        //}
        
        [communityTabBarController presentViewController:chooseProfileImageViewController animated:YES completion:nil];
        
        //Controllo se devo scaricare l'immagine di facebook
        NSString *fb_login = [Utility getDefaultValueForKey:USER_FACEBOOK_LOGIN];
        if(![fb_login isEqualToString:@""] && fb_login != nil) {
            
            NSString *url_image = [NSString stringWithFormat:@"http://graph.facebook.com/%@",fb_login];
            url_image = [url_image stringByAppendingString:@"/picture?type=normal"];
            
            NSData *imageProfileData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url_image]];
            chooseProfileImageViewController.imgProfile.image = [[UIImage imageWithData:imageProfileData] retain];
        }
    }
}

- (void)initializeTabBar {
    friendsViewController = [[FriendsViewController alloc] initWithNibName:@"FriendsViewController" bundle:[NSBundle mainBundle]];
    
    myViewController = [[MyViewController alloc] initWithNibName:@"MyViewController" bundle:[NSBundle mainBundle]];
    
    pointsViewController = [[PointsViewController alloc] initWithNibName:@"PointsViewController" bundle:[NSBundle mainBundle]];
    
    otherViewController = [[More alloc] initWithNibName:@"More" bundle:[NSBundle mainBundle]];
    
    NSArray *viewControllers = @[friendsViewController, myViewController, pointsViewController, otherViewController];
    NSMutableArray *controllersAndNav = [[NSMutableArray alloc]init];
    for (int i=0; i < viewControllers.count; i++) {
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:viewControllers[i]];
        [controllersAndNav addObject:navController];
    }
    communityTabBarController = [[CommunityTabBarControllerViewController alloc] init];
    communityTabBarController.viewControllers = controllersAndNav;
   
    //set the tab bar controller background image    
//    communityTabBarController.tabBar.backgroundImage = [UIImage imageNamed:@"tab_bar.png"];
    
    //Se devo visualizzare la scelta foto lo faccio subito
    communityTabBarController.modalPresentationStyle = UIModalPresentationFullScreen;
    communityTabBarController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    UITabBar *tabBar = communityTabBarController.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
    
    [tabBarItem1 setFinishedSelectedImage:[UIImage imageNamed:@"tab_friends"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab_friends"]];
    [tabBarItem2 setFinishedSelectedImage:[UIImage imageNamed:@"tab_me"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab_me"]];
    [tabBarItem3 setFinishedSelectedImage:[UIImage imageNamed:@"tab_points"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab_points"]];
    [tabBarItem4 setFinishedSelectedImage:[UIImage imageNamed:@"tab_other"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab_other"]];
        
    tabBarItem1.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    tabBarItem2.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    tabBarItem3.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    tabBarItem4.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    
    [self presentViewController:communityTabBarController animated:NO completion:nil];
}

-(void)alertView:(CustomAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        if(buttonIndex == 1) {
            //Apri una pagina che invia un email con il link di conferma
            [self loginWithSendConfirm:YES];
        }
    } else if (alertView.tag == 1001) {
        if (buttonIndex != alertView.cancelButtonIndex) {
            NSURL *presentationUrl = [NSURL URLWithString:URL_Presentation];
            if ([[UIApplication sharedApplication] canOpenURL:presentationUrl]) {
                [[UIApplication sharedApplication] openURL:presentationUrl];
            }
        }
    }
}


//Disabilita i comandi e attiva il loading grafico
-(void)setModeLoading:(BOOL)active {
    if(active) {
        if(loadView == nil) {
            loadView = [[GBLoadingView alloc] initWithFrame:CGRectMake(320/2-160/2, (480-64)/2-125/2, 160, 125) opacity:1.0 message:@"Login.." cornerRadius:10.0 activityStyle:UIActivityIndicatorViewStyleWhiteLarge];
            loadView.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0 alpha:0.7];
        }
        [self.view addSubview:loadView];
        [self.view setUserInteractionEnabled:NO];
        btLogin.enabled = NO;
    } else {
        [loadView removeFromSuperview];
        [self.view setUserInteractionEnabled:YES];
        btLogin.enabled = YES;
    }
}

- (void)getUserDetails {
    if (FBSession.activeSession.isOpen) {
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection,
           NSDictionary<FBGraphUser> *user,
           NSError *error) {
             if (!error) {
                 
                 //A questo punto prendo le informazioni che mi interessano e le invio al server
                 //che mi registra con facebook e mi logga. Nel caso fossi già registrato con facebook mi logga
                 //soltanto
                 NSDictionary *dict = (NSDictionary *)user;
                 
                 NSString *name = [Utility encryptString:user.first_name];
                 NSString *surname = [Utility encryptString:user.last_name];
                 NSString *gender = [Utility encryptString:[dict valueForKey:@"gender"]];
                 NSString *birthday = [Utility encryptString:user.birthday];
                 NSString *profile_id = user.id;
                 profile_id = [Utility encryptString:profile_id];
                 
                 //Faccio la registrazione e login
                 NSString *device = [Utility encryptString:[Utility getDeviceAppId]];
                 NSString *token = [Utility encryptString:[Utility getDeviceToken]];

                 //Creo la stringa di inserimento
                 NSString *str = [URL_SERVER stringByAppendingString:@"fb_login.php"];
                 
                 //Start parser thread
                 if(loginParser == nil) {
                     loginParser = [[RequestParser alloc] init];
                 }
                 [loginParser setStrURL:str];
                 [loginParser addPostValue:name forKey:@"name"];
                 [loginParser addPostValue:surname forKey:@"surname"];
                 [loginParser addPostValue:profile_id forKey:@"facebook"];
                 [loginParser addPostValue:device forKey:@"device"];
                 [loginParser addPostValue:token forKey:@"token"];
                 [loginParser addPostValue:birthday forKey:@"birthday"];
                 [loginParser addPostValue:gender forKey:@"gender"];
                 loginParser.delegate = self;
                 [loginParser start];
                 
                 [self setModeLoading:NO];
             }
         }];
    }
}

-(void)dealloc {
    [chooseProfileImageViewController release];
    [myViewController release];
    [friendsViewController release];
    [communityTabBarController release];
    [loadView release];
    [btJoin release];
    [joinViewC release];
    [_btSignIn release];
    [super dealloc];
}

@end
