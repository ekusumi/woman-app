//
//  MasterViewController.h
//  suregift
//
//  Created by Matteo Gobbi on 19/12/12.
//  Copyright (c) 2012 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestParser.h"
#import "FriendsViewController.h"
#import "MyViewController.h"
#import "JoinViewController.h"
#import "ChooseProfileImageViewController.h"
#import "CommunityTabBarControllerViewController.h"
#import "PointsViewController.h"
#import "More.h"

@interface MasterViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, ThreadCallBack, UIAlertViewDelegate> {
    RequestParser *loginParser;
    GBLoadingView *loadView;
    UIBarButtonItem *btLogin;
    
    FriendsViewController *friendsViewController;
    MyViewController *myViewController;
    PointsViewController *pointsViewController;
    More *otherViewController;
    
    CommunityTabBarControllerViewController *communityTabBarController;
    
    ChooseProfileImageViewController *chooseProfileImageViewController;
}

@property (nonatomic, retain) IBOutlet UIImageView *backgroundView;

@property (nonatomic, retain) IBOutlet UIButton *btJoin;
@property (retain, nonatomic) IBOutlet UIButton *btSignIn;

@property (nonatomic, retain) IBOutlet UIImageView *usernameTxtBkgd;
@property (nonatomic, retain)  UITextField *usernameTextfield;
@property (nonatomic, retain) IBOutlet UIImageView *passwordTxtBkgd;
@property (nonatomic, retain)  UITextField *passwordTextfield;

@property (nonatomic, retain) JoinViewController *joinViewC;

-(IBAction)login:(id)sender;
-(IBAction)join:(id)sender;

-(void)loginWithSendConfirm:(BOOL)send;

-(void) notifyThreadEnd:(NSDictionary *)responseDict;

-(void)setModeLoading:(BOOL)active;

-(void)goToCommunity;

-(void)getUserDetails;

@end
