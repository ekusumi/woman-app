//
//  SignupViewController.h
//  SocialLogin
//
//  Created by Me on 8/12/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupViewController : UIViewController <UIWebViewDelegate> {
    IBOutlet UIWebView *webView;
}

@end
