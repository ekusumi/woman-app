//
//  LoadingView.h
//  HealthInHand
//
//  Created by Razvan on 12/19/12.
//  Copyright (c) 2012 Creatio. All rights reserved.
//

#import <UIKit/UIKit.h>

// =============================================================================
// LoadingView Public Interface
// =============================================================================
@interface LoadingView : UIView

//Public methods
- (LoadingView *)view;
- (void)centerContents;
- (void)show;
- (void)hide;

@end
