//
//  LoadingView.m
//  HealthInHand
//
//  Created by Razvan on 12/19/12.
//  Copyright (c) 2012 Creatio. All rights reserved.
//

#import "LoadingView.h"

// =============================================================================
// LoadingView Private Interface
// =============================================================================
@interface LoadingView ()

//IBOutlets
@property (nonatomic, retain) IBOutlet UIView *loadingContainer;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;

@end


// =============================================================================
// LoadingView Implementation
// =============================================================================
@implementation LoadingView

- (LoadingView *)view {
    // Load nib named the same as our custom class
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
    
    return [[nib instantiateWithOwner:nil options:nil] objectAtIndex:0];
}


// -----------------------------------------------------------------------------
// Public methods
// -----------------------------------------------------------------------------
#pragma mark - Public methods
- (void)centerContents{
    CGRect frame = [self superview].frame;
    frame.origin.x = 0.0;
    frame.origin.y = 0.0;
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        frame.size.height -= 93.0;
    } else {
        // code for 3.5-inch screen
        frame.size.height -= 193.0;
    }

    [self setFrame:frame];
    
    CGFloat midY = (self.frame.size.height - self.loadingContainer.frame.size.height) / 2;
    self.loadingContainer.frame = (CGRect){.origin= CGPointMake(0.0f, midY),
                                           .size = self.loadingContainer.frame.size};
}


- (void)show {
    [self.activityIndicator startAnimating];
    
    [UIView transitionWithView:self
                      duration:0.01
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self setAlpha:1.0];
                    }
                    completion:NULL];
}


- (void)hide {
    [UIView transitionWithView:self
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self setAlpha:0.0];
                    }
                    completion:^(BOOL finished){
                        [self.activityIndicator stopAnimating];
                    }];
}

@end
