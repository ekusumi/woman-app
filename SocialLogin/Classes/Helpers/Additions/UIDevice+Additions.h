//
//  UIDevice+Additions.h
//  HealthInHand
//
//  Created by Razvan on 12/14/12.
//  Copyright (c) 2012 Creatio. All rights reserved.
//

#import <UIKit/UIKit.h>

// =============================================================================
// UIDevice+Additions Public Interface
// =============================================================================
@interface UIDevice (Additions)

//Public methods
- (NSString *)uniqueDeviceIdentifier;
- (NSString *)uniqueGlobalDeviceIdentifier;
- (BOOL)hasFourInchDisplay;

@end
