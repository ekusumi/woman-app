//
//  UIDevice+Additions.m
//  HealthInHand
//
//  Created by Razvan on 12/14/12.
//  Copyright (c) 2012 Creatio. All rights reserved.
//

#import "UIDevice+Additions.h"
#import "NSString+Additions.h"

#include <sys/socket.h> // Per msqr
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>

// =============================================================================
// UIDevice+Additions Private Interface
// =============================================================================
@interface UIDevice(Private)

//Private methods
- (NSString *)macaddress;

@end


// =============================================================================
// UIDevice+Additions Implementation
// =============================================================================
@implementation UIDevice (Additions)


// -----------------------------------------------------------------------------
// Private methods
// -----------------------------------------------------------------------------
#pragma mark - Private methods
// Return the local MAC addy
- (NSString *) macaddress {
    
    int                 mib[6];
    size_t              len;
    char                *buf;
    unsigned char       *ptr;
    struct if_msghdr    *ifm;
    struct sockaddr_dl  *sdl;
    
    mib[0] = CTL_NET;
    mib[1] = AF_ROUTE;
    mib[2] = 0;
    mib[3] = AF_LINK;
    mib[4] = NET_RT_IFLIST;
    
    if ((mib[5] = if_nametoindex("en0")) == 0) {
        printf("Error: if_nametoindex error\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, NULL, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 1\n");
        return NULL;
    }
    
    if ((buf = malloc(len)) == NULL) {
        printf("Could not allocate memory. error!\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, buf, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 2");
        return NULL;
    }
    
    ifm = (struct if_msghdr *)buf;
    sdl = (struct sockaddr_dl *)(ifm + 1);
    ptr = (unsigned char *)LLADDR(sdl);
    NSString *outstring = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
                           *ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5)];
    free(buf);
    
    return outstring;
}

// -----------------------------------------------------------------------------
// Public methods
// -----------------------------------------------------------------------------
#pragma mark - Public methods
- (NSString *)uniqueDeviceIdentifier {
    NSString *macaddress = [[UIDevice currentDevice] macaddress];
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    NSString *stringToHash = [NSString stringWithFormat:@"%@%@",macaddress,bundleIdentifier];
    NSString *uniqueIdentifier = [NSString MD5Hash:stringToHash];
    return uniqueIdentifier;
}


- (NSString *)uniqueGlobalDeviceIdentifier{
    NSString *macaddress = [[UIDevice currentDevice] macaddress];
    NSString *uniqueIdentifier = [NSString MD5Hash:macaddress];
    return uniqueIdentifier;
}


- (BOOL)hasFourInchDisplay {
    return ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone &&
                                                             [UIScreen mainScreen].bounds.size.height == 568.0);
}

@end
