//
//  NSString+Additions.h
//  HealthInHand
//
//  Created by Razvan on 12/14/12.
//  Copyright (c) 2012 Creatio. All rights reserved.
//

#import <Foundation/Foundation.h>

// =============================================================================
// NSString+Additions Public Interface
// =============================================================================
@interface NSString (Additions)

//Public methods
+ (BOOL)isNilOrEmpty:(NSString *)string;
+ (BOOL) validateEmail: (NSString *)candidate;
+ (NSString *)MD5Hash:(NSString *)string;
+ (NSString *)sha256:(NSString *)clear;
+ (NSString *)urlEncode:(NSString *)url;

- (NSComparisonResult)compareAsNumbers:(NSString *)otherString;
- (NSString *)formattedPhoneNumber;
- (NSString *)stringByConvertingHTMLToPlainText;
- (NSString *) stringByStrippingHTML:(NSString *)htmlStr;
@end
