//
//  UIColor-Additions.m
//  vevoke
//
//  Created by Andrei Vig on 11/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "UIColor-Additions.h"


@implementation UIColor (UIColor_Additions)

+ (UIColor *)backgroundColor {
	return [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]];
}

+ (UIColor *)blackNavColor {
	return [UIColor blackColor];
}

+ (UIColor *)tealGreenColor {
	return [UIColor colorWithRed:0.0 green:0.5 blue:0.5 alpha:1.0];
}

+ (UIColor *)lightAppColor {
    return [UIColor colorWithRed:0.900 green:0.898 blue:0.898 alpha:1.0];
}

+ (UIColor *)navAppColor {
    return [UIColor colorWithRed:44/255.0f green:58/255.0f blue:106/255.0f alpha:1.0];    
}


@end

