//
//  NSString+Additions.m
//  HealthInHand
//
//  Created by Razvan on 12/14/12.
//  Copyright (c) 2012 Creatio. All rights reserved.
//

#import "NSString+Additions.h"
#import <CommonCrypto/CommonDigest.h>

// =============================================================================
// UIDevice+Additions Implementation
// =============================================================================
@implementation NSString (Additions)

+ (BOOL)isNilOrEmpty:(NSString *)string {
	
    if ([string isKindOfClass:[NSNull class]]) return YES;
    
	BOOL isNilOrEmpty = NO;
	
	if ((nil == string) || ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])) {
		isNilOrEmpty = YES;
	}
	
	return isNilOrEmpty;
	
}


+ (BOOL) validateEmail: (NSString *)candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}


+ (NSString *)MD5Hash:(NSString *)string {
	
    const char *concat_str = [string UTF8String];
	
    unsigned char result[CC_MD5_DIGEST_LENGTH];
	CC_MD5(concat_str, strlen(concat_str), result);
	NSMutableString *hash = [NSMutableString string];
    
	for (int i = 0; i < 16; i++) {
        [hash appendFormat:@"%02X", result[i]];
	}
	
    return [hash lowercaseString];
}


+ (NSString *)sha256:(NSString *)clear {
    const char *s=[clear cStringUsingEncoding:NSASCIIStringEncoding];
    NSData *keyData=[NSData dataWithBytes:s length:strlen(s)];
    
    uint8_t digest[CC_SHA256_DIGEST_LENGTH]={0};
    CC_SHA256(keyData.bytes, keyData.length, digest);
    NSData *out=[NSData dataWithBytes:digest length:CC_SHA256_DIGEST_LENGTH];
    NSString *hash=[out description];
    hash = [hash stringByReplacingOccurrencesOfString:@" " withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];
    return hash;
}


+ (NSString *)urlEncode:(NSString *)url {
    NSString *encodedString = ( NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                  (CFStringRef)url,
                                                                                  NULL,
                                                                                  (CFStringRef)@"!*'();:@&=+$,./?%#[]-",
                                                                                  kCFStringEncodingUTF8);
    return encodedString;
}

- (NSComparisonResult)compareAsNumbers:(NSString *)otherString {
    NSNumber *selfNumber = [NSNumber numberWithInt:[self intValue]];
    NSNumber *otherNumber = [NSNumber numberWithInt:[otherString intValue]];
    
    NSComparisonResult finalResult;
    NSComparisonResult result = [selfNumber compare:otherNumber];
    
    if (NSOrderedAscending == result) {
        finalResult = NSOrderedAscending;
    } else if (NSOrderedDescending == result) {
        finalResult = NSOrderedDescending;
    } else {
        finalResult = result;
    }
    return finalResult;
}

- (NSString *)formattedPhoneNumber {
    
    return [self stringByReplacingOccurrencesOfString:@" " withString:@""];
}


// Strip HTML tags
- (NSString *)stringByConvertingHTMLToPlainText {
    
    // Character sets
    NSCharacterSet *stopCharacters = [NSCharacterSet characterSetWithCharactersInString:[NSString stringWithFormat:@"< \t\n\r%d%d%d%d", 0x0085, 0x000C, 0x2028, 0x2029]];
    NSCharacterSet *newLineAndWhitespaceCharacters = [NSCharacterSet characterSetWithCharactersInString:[NSString stringWithFormat:@" \t\n\r%d%d%d%d", 0x0085, 0x000C, 0x2028, 0x2029]];
    NSCharacterSet *tagNameCharacters = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"]; /**/
    
    // Scan and find all tags
    NSMutableString *result = [[NSMutableString alloc] initWithCapacity:self.length];
    NSScanner *scanner = [[NSScanner alloc] initWithString:self];
    [scanner setCharactersToBeSkipped:nil];
    [scanner setCaseSensitive:YES];
    NSString *str = nil, *tagName = nil;
    BOOL dontReplaceTagWithSpace = NO;
    do {
        
        // Scan up to the start of a tag or whitespace
        if ([scanner scanUpToCharactersFromSet:stopCharacters intoString:&str]) {
            [result appendString:str];
            str = nil; // reset
        }
        
        // Check if we've stopped at a tag/comment or whitespace
        if ([scanner scanString:@"<" intoString:NULL]) {
            
            // Stopped at a comment or tag
            if ([scanner scanString:@"!--" intoString:NULL]) {
                
                // Comment
                [scanner scanUpToString:@"-->" intoString:NULL];
                [scanner scanString:@"-->" intoString:NULL];
                
            } else {
                
                // Tag - remove and replace with space unless it's
                // a closing inline tag then dont replace with a space
                if ([scanner scanString:@"/" intoString:NULL]) {
                    
                    // Closing tag - replace with space unless it's inline
                    tagName = nil; dontReplaceTagWithSpace = NO;
                    if ([scanner scanCharactersFromSet:tagNameCharacters intoString:&tagName]) {
                        tagName = [tagName lowercaseString];
                        dontReplaceTagWithSpace = ([tagName isEqualToString:@"a"] ||
                                                   [tagName isEqualToString:@"b"] ||
                                                   [tagName isEqualToString:@"i"] ||
                                                   [tagName isEqualToString:@"q"] ||
                                                   [tagName isEqualToString:@"span"] ||
                                                   [tagName isEqualToString:@"em"] ||
                                                   [tagName isEqualToString:@"strong"] ||
                                                   [tagName isEqualToString:@"cite"] ||
                                                   [tagName isEqualToString:@"abbr"] ||
                                                   [tagName isEqualToString:@"acronym"] ||
                                                   [tagName isEqualToString:@"label"]);
                    }
                    
                    // Replace tag with string unless it was an inline
                    if (!dontReplaceTagWithSpace && result.length > 0 && ![scanner isAtEnd]) [result appendString:@" "];
                    
                }
                
                // Scan past tag
                [scanner scanUpToString:@">" intoString:NULL];
                [scanner scanString:@">" intoString:NULL];
                
            }
            
        } else {
            
            // Stopped at whitespace - replace all whitespace and newlines with a space
            if ([scanner scanCharactersFromSet:newLineAndWhitespaceCharacters intoString:NULL]) {
                if (result.length > 0 && ![scanner isAtEnd]) [result appendString:@" "]; // Dont append space to beginning or end of result
            }
            
        }
        
    } while (![scanner isAtEnd]);
    
    // Decode HTML entities and return
    NSString *retString = [result stringByDecodingHTMLEntities];
    return retString;
}

// Decode all HTML entities using GTM
- (NSString *)stringByDecodingHTMLEntities {
    // gtm_stringByUnescapingFromHTML can return self so create new string ;)
    return [NSString stringWithString:[self gtm_stringByUnescapingFromHTML]];
}

- (NSString *) stringByStrippingHTML:(NSString *)htmlStr {
    
    NSRange range;
    while ((range = [htmlStr rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        htmlStr = [htmlStr stringByReplacingCharactersInRange:range withString:@""];
    htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"&amp;" withString:@""];
    return htmlStr;
}


@end
