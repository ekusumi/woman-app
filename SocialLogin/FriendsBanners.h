//
//  FriendsBanners.h
//  SocialLogin
//
//  Created by sebastian.achim on 7/10/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FriendsBanners : NSObject

@property (nonatomic, retain) NSString    *bannersURL;

- (id)initWithBanners:(NSDictionary *)info;

@end
