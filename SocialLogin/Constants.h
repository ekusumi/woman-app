//
//  Constants.h
//  suregift
//
//  Created by Matteo Gobbi on 11/01/13.
//  Copyright (c) 2013 Matteo Gobbi. All rights reserved.
//


//Change this with your key (also in PHP source code)
#define kKey @"a16byteslongkey!a16byteslongkey!"

//Change this with your address backend
#define URL_SERVER @"http://182.48.52.97/00matikon/"

#define kAPIBaseURLString @"http://182.48.52.97/00matikon/"

#define URL_Presentation @"http://182.48.52.97/matikonportal/users/news"
#define URL_Search @"http://182.48.52.97/kyba/events/sindan" // @"http://iphone.lparty.jp/events/json"

//Device
#define DEVICE_APP_ID @"DeviceAppId"
#define DEVICE_PUSH_TOKEN @"DeviceToken"

//User
#define USER_SESSION @"UserSession"
#define USER_FACEBOOK_LOGIN @"UserFacebookLogin"
#define USER_IMG_PROFILE @"UserImageProfile"

//Float and dimensions
#define IMG_PROFILE_SCALE_W 65
#define IMG_PROFILE_SCALE_H 65
#define IMG_PROFILE_QUALITY 1.0

//Services
#define SERVICE_CHANGE_IMG_PROFILE @"change_image_profile"

//Error
#define ERROR_KEY @"error"
#define ERROR_CONNECTION @"-2"

//Others
#define IMG_PROFILE_NAME @"default_profile.png"

#define APP_FONT @"AvenirNextCondensed-Medium"
#define APP_TEXT_COLOR_GRAY 100.0/255.0

//Content
#define MoreCells @"favorites#お気に入り;url#http://revo-start.asia/#運営会社;text#\n1．著作権当ウェブサイトに記載されている内容の著作権は、NRIグループ及びコンテンツの作者に帰属します。当ウェブサイト及びそのコンテンツを使用できるのは、著作権法上「私的使用のための複製」及び「引用」などの場合に限られています。この場合、出典を明記してください。なお、「私的使用のための複製」や「引用」の範囲を超える場合には、NRIの使用許諾が必要になります。使用許諾のお申し込みやお問い合わせは、NRIコーポレートコミュニケーション部（Tel.03-6270-8100／information@nri.co.jp）にご連絡ください。\n\n2．リンク当ウェブサイトへのリンクは自由です。ただし、違法または公序良俗に反するとNRIが考えるサイトからのリンクや、当ウェブサイトへのリンクであるということが不明確になる手段でのリンクについてはお断りすることがあります。トップページ以外にリンクされた場合には、そのページのコンテンツやURLが、予告なしに変更又は廃止される可能性があることをご了解ください。\n\n3．準拠法および管轄裁判所当ウェブサイトおよび「サイト利用規定」の解釈および適用は、日本国法に準拠します。また、当ウェブサイトに関わる全ての紛争については、他に別段の定めのない限り、東京地方裁判所を第一審の専属管轄裁判所とします。\n\n4．対応ブラウザ当ウェブサイトを閲覧する際には、Microsoft Internet Explorer 6.0以上、Firefox 3.0以上のブラウザの使用を推奨します。これら推奨ブラウザ以外でご覧いただく場合、画面の一部が正しく表示されないことがありますので、ご了解ください。\n\n5．プラグイン当ウェブサイトに収録されているPDFファイルをご覧いただくには、Adobe Readerが必要です。当ウェブサイトに収録されている一部コンテンツはFlashが使われております。これらのコンテンツをご覧いただくためには、Adobe Flashが必要です。この他、コンテンツによっては別途プラグインが必要となる場合もあります。各コンテンツのプラグイン情報等をご参照ください。\n\n6．お問い合わせ先当ウェブサイト・当社についてご意見・ご質問などがある場合は、「お問い合わせ」ページをご確認の上、各担当までメールにてご連絡下さい。お問い合わせ先が不明の場合は、NRIコーポレートコミュニケーション部（Tel.03-6270-8100）にご連絡ください。\n\n\n\n\n\n#利用規定;url#http://coltboy.com#名前を変更する;none#ポイントを見る;none#お問い合わせ"

#define APP_TEXT_COLOR_GRAY 100.0/255.0
#define APP_TEXT_COLOR_PINK [UIColor colorWithRed:228.0/255.0 green:114.0/255.0 blue:151.0/255.0 alpha:1.0]

//FirstTab Strings
#define FriendsTabTitle @"Friends"
#define FriendsTabTopRightButtonTitle @"FWD"
#define FriendsTabLogoutButtonTitle @"Logout"
#define FriendsTabPRButtonTitle @"PR"

//Location View Controller
#define PrefectureTabTitle @"地域検索"
#define PrefectureListEntry @"東京;神奈川;埼玉"

#define Tokyo @"東京都全て;23区;東京都23区以外の市;新宿区;港区;渋谷区;台東区;中野区;豊島区;目黒区;千代田区;中央区;文京区;大田区"
#define Kanagawa @"神奈川県全て;横浜市;川崎市;相模原市;横浜市・川崎市・相模原市以外の市;神奈川県の中区;横浜市の金沢区;横浜市鶴見区;横浜市西区;横浜市南区;その他"
#define Saitama @"埼玉県全て;さいたま市;さいたま市以外の市;さいたま市西区;さいたま市中央区;さいたま市桜区;さいたま市浦和区;さいたま市南区;さいたま市緑区;川越市;その他"


//Stations View Controller
#define StationTabTitle @"待遇から検索"
#define Stations @"オープニング;ヘアメイク有;ドレス貸与;送りあり;ノルマなし;寮完備;制服貸与;日払い"

#define StationsEntry1 @"a1;a2;a3;a4;a5"
#define StationsEntry2 @"b1;b2;b3;b4;b5"
#define StationsEntry3 @"c1;c2;c3;c4;d5"
#define StationsEntry4 @"d1;d2;d3;d4;d5"
#define StationsEntry5 @"e1;e2;e3;e4;e5"
#define StationsEntry6 @"f1;f2;f3;f4;f5"
#define StationsEntry7 @"g1;g2;g3;g4;g5"
#define StationsEntry8 @"h1;h2;h3;h4;h5"

#define CheckStation1 @"a1;a2;a3;a4;a5"
#define CheckStation2 @"b1;b2;b3;b4;b5"
#define CheckStation3 @"c1;c2;c3;c4;d5"
#define CheckStation4 @"d1;d2;d3;d4;d5"
#define CheckStation5 @"e1;e2;e3;e4;e5"

//Selection View Controller
#define SelectionListEntry @"7/30;7/31;8/01;8/02;8/03;8/04;8/05;8/02"

#define Selection1 @"Select All Rows;a1;a2;a3;a4;a5;a6;a7;a8;a9;a10"
#define Selection2 @"Select All Rows;b1;b2;b3;b4;b5;b6;b7;b8;b9;b10"
#define Selection3 @"Select All Rows;c1;c2;c3;c4;c5;c6;c7;c8;c9;c10"

//UIAlertView
#define AlertTitle @""
#define AlertMessage @"You selected all the rows at once. You can only unselec them all at once by pressing Select All row again."
#define AlertMessage2 @"You cannot select more than 3 options."
#define AlertMessage3 @"Please select at least one option to be able to perform the search!"

//chat view controller
#define ChatControllerTitleView @"職種から探す"
#define ChatCellTitles @"夜キャバクラ;昼キャバクラ;朝キャバクラ;ガールズバー;スナック;セクハラキャバクラ;その他"
#define Chat1 @"夜キャバクラ全て;a1;a2"
#define Chat2 @"昼キャバクラ全て;b1;b2;b3;b4;b5"
#define Chat3 @"朝キャバクラ全て;c1;c2;c3;c4;c5;c6"
#define Chat4 @"ガールズバー全て;d1;d2;d3;d4;d5;d6;d7"
#define Chat5 @"スナック全て;e1;e2;e3;e4"
#define Chat6 @"メイド喫茶全て;f1;f2;f3;f4;f5;f6"
#define Chat7 @"その他全て;g1;g2;g3;g3;g3;g3;g3;g3;g3;g3;g3;g3;g3;g3;g3;g3;g3;g3"


#define LoginViewTitle @"Login"
#define SearchControllerTitle @"Search"
#define MenuViewTitle @"Friends"
#define UnderMenuButtonTitle @"Title"
#define SignUpTitle @"Sign Up"

#define SIGN_UP_URL @"http://182.48.52.97/00matikon/signup"
#define APPLY_URL @"http://182.48.52.97/00matikon/users/apply"
